//
//  CacheManagerTests.m
//  NetworkLayer
//
//  Created by VS on 03/09/16.
//  Copyright © 2016 VS. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "CacheManager.h"
#import "TestBrandListMapper.h"
#import "CacheManager+ReadCache.h"

#define kCorrectFileName @"temp.json"
#define kIncorrectFileName @"temp1.json"
#define kEmptyString @""
#define kEmptyFileName kEmptyString
#define kJsonString @"{ \"result\": { \"data\": [ {\"result\": \"Vectoscalar\"}]} }"

@interface CacheManagerTests : XCTestCase {
    
    NSString *directoryPath;
    CacheManager *cacheManager;
    NSString *correctFilePath;
}

- (BOOL)readCacheAtFilePath:(NSString *)filePath;

@end

@implementation CacheManagerTests

- (void)setUp {
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
     cacheManager = [[CacheManager alloc] init];
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}


- (void)testReadCacheAtFilePath {
    
    [self writeJsonToCache];
    
    NSString *incorrectFilePath1 = [directoryPath
                                    stringByAppendingPathComponent:kIncorrectFileName];
    NSString *incorrectFilePath2 = [directoryPath
                                    stringByAppendingPathComponent:kEmptyFileName];
    
    BOOL result = [self readCacheAtFilePath:correctFilePath];
    XCTAssertEqual(result, YES);
    
    result = [self readCacheAtFilePath:incorrectFilePath1];
    XCTAssertEqual(result, NO);
    
    result = [self readCacheAtFilePath:incorrectFilePath2];
    XCTAssertEqual(result, NO);
    
}


#pragma mark - Private Helper methods

- (void)writeJsonToCache {
    
    directoryPath =  [CacheManager apiCacheDirectoryPath];
    correctFilePath = [directoryPath stringByAppendingPathComponent:kCorrectFileName];
    
    NSString *response = kJsonString;
    NSError *error;
    [response writeToFile:correctFilePath
               atomically:true
                 encoding:NSUTF8StringEncoding
                    error:&error];
    
    if(error)
    {
        NSLog(@"Error in writing file at path = %@ \n Error = %@", correctFilePath, error);
    }
    else
    {
        NSLog(@"Successfully write Json at path :%@", correctFilePath);
    }
    
}



- (BOOL)readCacheAtFilePath:(NSString *)filePath {
    
     TestBrandListMapper *mapper = [[TestBrandListMapper alloc] init];
    __block BOOL result;
    
    [cacheManager readCacheForMapperClass:[mapper class]
                               atFilePath:filePath
                               completion:^(id jsonObject, NSError *error) {
                                   if (jsonObject) {
                                       result = YES;
                                   }
                                   else if (error) {
                                       result = NO;
                                   }
                               }];
    return result;
}

@end
