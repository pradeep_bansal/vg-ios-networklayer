//
//  BrandListMapper.h
//  Test
//
//  Created by VS on 02/09/16.
//  Copyright © 2016 VS. All rights reserved.
//

#import <Foundation/Foundation.h>

@class TestBrandResultMapper;

@class TestBrand;



@interface TestBrandListMapper : NSObject

@property(strong, nonatomic) TestBrandResultMapper *result;

@end


@interface TestBrandResultMapper : NSObject

@property(strong, nonatomic) NSString *httpStatus;
@property(assign, nonatomic) BOOL status;
@property(strong, nonatomic) NSString *msg;
@property(strong, nonatomic) NSArray<TestBrand *> *data;

@end


@interface TestBrand : NSObject

@property(strong, nonatomic) NSString *name;
@property(strong, nonatomic) NSString *link_rewrite;
@property(assign, nonatomic) BOOL is_popular;
@property(strong, nonatomic) NSString *logo;
@property(strong, nonatomic) NSString *count;
@property(strong, nonatomic) NSString *brand_id;


@end