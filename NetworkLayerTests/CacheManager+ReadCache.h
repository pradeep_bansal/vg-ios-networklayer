//
//  CacheManager+ReadCache.h
//  NetworkLayer
//
//  Created by VS on 03/09/16.
//  Copyright © 2016 VS. All rights reserved.
//

#import "CacheManager.h"

typedef void (^ResponseBlock)(NSObject *mapper, NSError *error);
typedef void (^CacheResponseBlock) (id jsonObject, NSString *hashString, NSError *error);

@interface CacheManager (ReadCache)

- (void)readCacheForMapperClass:(Class)mapperClass
                     atFilePath:(NSString *)filePath
                     completion:(CacheResponseBlock)completionBlock;

+ (NSString *)apiCacheDirectoryPath;

@end
