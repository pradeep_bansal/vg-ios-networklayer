//
//  main.m
//  NetworkLayerDemo
//
//  Created by Saurabh Goyal on 3/31/17.
//  Copyright © 2017 VS. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
