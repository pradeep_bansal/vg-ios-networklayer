//
//  ViewController.m
//  NetworkLayerDemo
//
//  Created by Saurabh Goyal on 3/31/17.
//  Copyright © 2017 VS. All rights reserved.
//

#import "ViewController.h"
#import <NetworkLayer/NetworkLayer.h>
#import "WAServerCommunicator.h"
#import "APIResponseModel.h"
#import "CDHomeApiMapper.h"
#import "CompareSimilarCarsMapper.h"
#import "NCBrandResponceApiModel.h"
#import "OANewsDetailModel.h"


//#define kBaseURL            @"http://newcarsapi.gaadi.com/v1/"
//#define kBaseURL            @"https://www.cardekho.com/api/v1/"
#define kBaseURL            @"https://stagingweb.cardekho.com/api/v1/"
//#define kBaseURL            @"http://testing.cardekho.com/api/v1/"
#define kCitiesURL          kBaseURL @"cities"
#define kModelDetailsURL    kBaseURL @"model/overview"
#define kCompareDetailsURL    kBaseURL @"compare/index"
#define kHomeURL            kBaseURL @"site/home"
#define kSmilerCarUrl   kBaseURL @"model/similarCars"
#define kGetBrandsUrl           kBaseURL @"brands"
#define kNewsDetailsURL         kBaseURL @"news/detail"



@interface ViewController (){
    NetworkManager *networkManager;
}

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    [CacheManager setAllowedCacheSize:1*1024*1024];
    networkManager = [[NetworkManager alloc] init];
    [NetworkManager setServerRequestCommonParameters:@{
                                                       @"langCode" : @"en",
                                                       @"countryCode" : @"in",
                                                       @"businessUnit" : @"car",
                                                       @"domain" : @"2",
                                                       @"Content-Type" : @"application/json",
                                                       @"devicePlatform" : @"iOS",
                                                       @"appVersion" : @"7.4.8"
                                                       }];
    
//    [NetworkManager setRequestHeaderWithDictionary:@{
//                                                     @"langCode" : @"en",
//                                                     @"countryCode" : @"in",
//                                                     @"businessUnit" : @"car",
//                                                     @"domain" : @"cardekho",
//                                                     @"Content-Type" : @"application/json",
//                                                     @"_format" : @"json",
//                                                     @"devicePlatform" : @"iOS"
//                                                     }];
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)testApiMethod:(id)sender {
//    [networkManager getServerDataWithUrl:@"https://www.cardekho.com/api/v1/cities"
//                            requestParameters:nil
//                               bodyParameters:nil
//                                apiMethodType:@"GET"
//                               andMapperClass:[NSDictionary class]
//                                   completion:^(NSObject *mapper, NSError *error) {
//                                       
//                                       NSLog(@":::::%@", mapper);
//                                       
//                                   }];
//    [networkManager getServerDataWithUrl:kCitiesURL
//                       requestParameters:nil
//                          bodyParameters:nil
//                           apiMethodType:@"GET"
//                          andMapperClass:[APIResponseModel class]
//                              completion:^(NSObject *mapper, NSError *error) {
//                                  
//                                  NSLog(@":::::%@", mapper);
//                                  
//                              }];
    
    NSDictionary *info = @{
        @"apisourcekey" : @"used-car-api",
        @"filter" : @"all",
        @"device" : @"app",
        @"domain" : @"2",
        @"sort_by":@"goodness_score",
        @"city_id[]":@"125",
        @"make_id[]" : @"22"
        };
    
    info = @{
             @"used_car_id":@"1386559",
             @"device":@"ios",
             @"apisourcekey":@"used-car-api",
             @"domain":@"2"
             };
    
    [networkManager getServerDataWithUrl:@"http://api.alphausedcar.gaadi.com/v1/vdp/getUsedCarFullDetails"
                       requestParameters:nil
                          bodyParameters:info
                           apiMethodType:@"POST"
                          andMapperClass:[NSDictionary class]
                           andSerializer: NetworkManagerSerializerHTTP
                              completion:^(NSObject *mapper, NSString *authorization, NSError *error) {
                                  NSLog(@":::::%@", mapper);
                              }];
    
}


- (IBAction)testApiMethod1:(id)sender {
    
    //[self modelDetailLink];
   // [self checkHomeData];
   // [self checkSimilerDataData];
//    [self checkBrandsDataData];
//    [self checkNewsDetailsData];
    
//    NSDictionary *dictParm = @{
//        @"apisourcekey" : @"used-car-api",
//        @"filter" : @"all",
//        @"device" : @"app",
//        @"domain" : @"1"
//        };
//    [networkManager getServerDataWithUrl:@"http://api.betausedcar.gaadi.com/v1/used-cars/getTopUsedCarFilters"
//                       requestParameters:nil
//                          bodyParameters:dictParm
//                           apiMethodType:@"POST"
//                          andMapperClass:[APIResponseModel class]
//                              completion:^(NSObject *mapper, NSError *error) {
//                                  
//                                  NSLog(@":::::%@", mapper);
//                                  
//                              }];    
    
//    NSDictionary *dictParm = @{
//                               @"brandSlug" : @"maruti",
//                               @"modelSlug" : @"swift dzire",
//                               @"formatPrice" : @"0"
//                               };
//    [networkManager getServerDataWithUrl:kModelDetailsURL
//                       requestParameters:dictParm
//                          bodyParameters:@[@"variants",@"images",@"videos", @"colors", @"similarCars", @"news"]
//                           apiMethodType:@"POST"
//                          andMapperClass:[APIResponseModel class]
//                              completion:^(NSObject *mapper, NSError *error) {
//                                  
//                                  NSLog(@":::::%@", mapper);
//                                  
//                              }];
    
//    NSArray *dictParm = @[@{
//        @"brand" : @"maruti---",
//        @"model" : @"wagon-r",
//        @"variant" : @"vxi-bs-iv"
//    },
//    @{
//        @"brand" : @"hyundai",
//        @"model" : @"i20",
//        @"variant" : @"1.4-era"
//    }];
//    
//    [networkManager getServerDataWithUrl:kCompareDetailsURL
//                       requestParameters:nil
//                          bodyParameters:dictParm
//                           apiMethodType:@"POST"
//                          andMapperClass:[APIResponseModel class]
//                              completion:^(NSObject *mapper, NSError *error) {
//                                  
//                                  NSLog(@":::::%@", mapper);
//                                  
//                              }];
    
    
//    [WAServerCommunicator GetDataForMethod:@"cities?Content-Type=application/json&businessUnit=car&countryCode=in&domain=gaadi&langCode=en" withParameters:@{} onCompletion:^(BOOL status, NSError *error, NSDictionary *responseData) {
//        
//    }];
    
//    NSDictionary* dict=@{
//                         @"brandSlug" :@"maruti",
//                         @"modelSlug" :@"baleno",
//                         };
//
//    [networkManager getServerDataWithUrl:@"http://www.cardekho.com/api/v1/model/ftc"
//                       requestParameters:dict
//                          bodyParameters:nil
//                           apiMethodType:@"GET"
//                          andMapperClass:[NSDictionary class]
//                              completion:^(NSObject *mapper, NSString *authorization, NSError *error) {
//
//                                  NSLog(@":::::%@", mapper);
//
//                              }];
    NSArray* dict=@[@"variants",@"images",@"videos", @"colors", @"similarCars", @"news",@"navs"];
    NSMutableDictionary *headerParam = [[NSMutableDictionary alloc] init];
    [headerParam setValue:@"" forKey:@""];
    [headerParam setValue:@"" forKey:@""];
    [NetworkManager setRequestHeaderWithDictionary:headerParam];
    
    [networkManager getServerDataWithUrl:@"https://www.cardekho.com/api/v1/model/overview"
                            requestParameters:nil
                               bodyParameters:dict
                                apiMethodType:@"POST"
                               andMapperClass:[NSDictionary class]
                                   completion:^(NSObject *mapper, NSString *authKey, NSError *error) {
                                       
                                       NSDictionary *modelRespose = (NSDictionary *)mapper;
                                       
                                       if(modelRespose && [modelRespose isKindOfClass:[NSDictionary class]])
                                       {
                                           
                                       }
                                       else
                                       {
                                           
                                       }
                                       
                                   }];
    
    
}

- (IBAction)loginVGMethod:(id)sender {
    NSDictionary *dict = @{@"info":
                               @{@"platform":@"iOS",
                                   @"deviceId":@"f0ce965520d72chj",
                                   @"eCode":@"",
                                   @"appVersion":@"3.0.0",
                                   @"appVersionCode":@(1),
                                   @"osVersion":@"22",
                                   @"deviceModel":@"MotoE2",
                                   @"sessionId":@"",
                                   @"encrypt":@"y",
                                   @"deviceManufacturer":@"motorola"},
                           @"data":@{
                               @"password":@"ankit@123",
                               @"ecode":@"gir_emp_ankit",
                               @"isForceLogin":@(false)}};
    
    
    dict = @{@"data" : @{},
    @"info" : @{
        @"deviceId" : @"1095D369-D595-4540-8EF1-E6D1E5AE9451",
        @"appVersion" : @"3.0.2",
        @"appVersionCode" : @"1",
        @"encrypt" : @"n",
        @"platform" : @"iOS",
        @"eCode" : @"GIR_EMP_DEEPAK",
        @"osVersion" : @"12.1",
        @"sessionId" : @"a4307840-62be-4b5f-ac18-0efd3aac0eb7",
        @"deviceManufacturer" : @"Apple",
        @"deviceModel" : @"Simulator"
        }};
    
    @try {
        long long timeStamp = ([[NSDate date] timeIntervalSince1970]*1000);
        NSString *timeStampString = [NSString stringWithFormat:@"%lld",timeStamp];
        NSMutableDictionary *headerParam = [[NSMutableDictionary alloc] init];
        [headerParam setValue:@"y" forKey:@"encrypt"];
        [headerParam setValue:timeStampString forKey:@"serverSyncTime"];
        [NetworkManager setRequestHeaderWithDictionary:headerParam];
    } @catch (NSException *exception) {
        
    } @finally {
        
    }
    
    [networkManager getServerDataWithUrl:@"https://hdfc.cardekho.com/UMS/v2/fetchBackgroundData?domain=2&appVersion=3.0.2&Content-Type=application/json&devicePlatform=iOS&country_code=in&lang_code=en&business_unit=car" requestParameters:nil bodyParameters:dict apiMethodType:@"POST" andMapperClass:[NSDictionary class] completion:^(NSObject *mapper, NSString *authorization, NSError *error) {
        NSLog(@"%@", mapper);
    }];
}

-(void)modelDetailLink
{
    NSDictionary *dictParm = @{
                               @"brandSlug" : @"maruti",
                               @"modelSlug" : @"vitara-brezza",
                               @"formatPrice" : @"0"
                               };
    [networkManager getServerDataWithUrl:kModelDetailsURL
                       requestParameters:dictParm
                          bodyParameters:@[@"variants",@"images",@"videos", @"colors", @"similarCars", @"news"]
                           apiMethodType:@"POST"
                          andMapperClass:[APIResponseModel class]
                              completion:^(NSObject *mapper, NSString *authorization, NSError *error) {
                                  APIResponseModel *apiResponse = (APIResponseModel*)mapper;
                                  
                                  NSLog(@":::Deepak::%@", apiResponse.data);
                                  
                              }];

}

-(void)checkHomeData
{
    [networkManager getServerDataWithUrl:kHomeURL
                       requestParameters:nil
                          bodyParameters:nil
                           apiMethodType:@"POST"
                          andMapperClass:[CDHomeApiMapper class]
                              completion:^(NSObject *mapper, NSString *authorization, NSError *error) {
                                  
                                  NSLog(@":::::%@", mapper);
                                  
                              }];
    
}

-(void)checkSimilerDataData
{
    NSDictionary* dict=@{
                         @"brandSlug" : @"maruti",
                         @"modelSlug" : @"baleno"
                         };
    [networkManager getServerDataWithUrl:kSmilerCarUrl
                       requestParameters:dict
                          bodyParameters:nil
                           apiMethodType:@"GET"
                          andMapperClass:[CompareSimilarCarsMapper class]
                              completion:^(NSObject *mapper, NSString *authorization, NSError *error) {
                                  
                                  NSLog(@":::::%@", mapper);
                                  
                              }];
    
}

-(void)checkBrandsDataData
{

    [networkManager getServerDataWithUrl:kGetBrandsUrl
                       requestParameters:nil
                          bodyParameters:nil
                           apiMethodType:@"GET"
                          andMapperClass:[NCBrandResponceApiModel class]
                              completion:^(NSObject *mapper, NSString *authorization, NSError *error) {
                                  NCBrandResponceApiModel *apiObj = (NCBrandResponceApiModel*)mapper;
                                  NSLog(@":::1::%ld", (long)apiObj.statusCode);
                                  NSLog(@"::2:::%@", apiObj.statusText);
                                  NSLog(@"::3:::%d", apiObj.status);
                                  NCBrand *obj =  [apiObj.data objectAtIndex:0];
                                   NSLog(@"::4:::%@",obj.slug);
                                  NSLog(@"::5:::%@",obj.name);
                              }];
}

-(void)checkNewsDetailsData
{
    NSDictionary *dict = @{
                           @"_format" : @"json",
                           @"slug" : @"limited-edition-mahindra-scorpio-adventure-launched-at-rs-13.07-lakh-18376.htm"
        };
    [networkManager getServerDataWithUrl:kNewsDetailsURL
                       requestParameters:dict
                          bodyParameters:nil
                           apiMethodType:@"GET"
                          andMapperClass:[OANewsDetailModel class]
                              completion:^(NSObject *mapper, NSString *authorization, NSError *error) {
                                  
                                  NSLog(@":::::%@", mapper);
                                  
                              }];
    
}




@end
