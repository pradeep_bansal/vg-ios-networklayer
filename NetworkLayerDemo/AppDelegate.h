//
//  AppDelegate.h
//  NetworkLayerDemo
//
//  Created by Saurabh Goyal on 3/31/17.
//  Copyright © 2017 VS. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

