//
//  NZServerCommunicator.h
//  NightLife
//
//  Created by Vasim Akram on 27/07/15.
//  Copyright (c) 2015 Vasim Akram. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef void (^CompletionHandler)(BOOL status, NSError *error, NSDictionary *responseData);

@interface WAServerCommunicator : NSObject

+(void)GetDataForMethod:(NSString *)serviceName withParameters:(NSDictionary *)param onCompletion:(CompletionHandler)handlar;
+(void)GetDataForMethod:(NSString *)serviceName withParameters:(NSDictionary *)param withFiles:(NSArray *)files onCompletion:(CompletionHandler)handlar;
@end
