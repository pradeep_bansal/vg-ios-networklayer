//
//  NZServerCommunicator.m
//  NightLife
//
//  Created by Vasim Akram on 27/07/15.
//  Copyright (c) 2015 Vasim Akram. All rights reserved.
//

#define BASEURL @"http://www.cardekho.com/api/v1"

#import "WAServerCommunicator.h"

@implementation WAServerCommunicator


+(void)GetDataForMethod:(NSString *)serviceName withParameters:(NSDictionary *)param onCompletion:(CompletionHandler)handlar{
    NSString *urlString = [NSString stringWithFormat:@"%@/%@",BASEURL,serviceName];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlString]];
    
    // Specify that it will be a POST request
    request.HTTPMethod = @"POST";
    
    // This is how we set header fields
    [request setValue:@"application/json; charset=utf-8" forHTTPHeaderField:@"Content-Type"];
    
    
    NSLog(@"Request URL : %@",urlString);
    
    NSData *requestBodyData = [NSJSONSerialization dataWithJSONObject:param options:NSJSONWritingPrettyPrinted error:nil];
    NSString *inputData = [[NSString alloc] initWithData:requestBodyData encoding:NSUTF8StringEncoding];
    NSLog(@"request Data : %@",inputData);
    
    
    request.HTTPBody = requestBodyData;
    
    request.timeoutInterval = 120.0;
    NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[requestBodyData length]];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    
    NSURLSession *defaultSession = [NSURLSession sessionWithConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    NSURLSessionDataTask *dataTask = [defaultSession dataTaskWithRequest:request completionHandler:^(NSData * data, NSURLResponse * response, NSError * connectionError) {
        if(data == nil){
            dispatch_async(dispatch_get_main_queue(), ^{
                handlar(NO, nil, @{@"ResponseMessage" : connectionError.userInfo[@"NSLocalizedDescription"]});
            });
            return;
        }
        
        NSString *strData = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
        NSLog(@"Got Response : %@",strData);
        
        NSError *error;
        NSDictionary *jsonResult = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&error];
        if (jsonResult != nil){
            
            if([[jsonResult objectForKey:@"statusCode"] integerValue] == 200){
                //Success
                dispatch_async(dispatch_get_main_queue(), ^{
                    handlar(YES, nil, jsonResult);
                });
                
            }
            else{
                //faliour
                dispatch_async(dispatch_get_main_queue(), ^{
                    handlar(NO, nil, jsonResult);
                });
                
            }
        }
        else if (error != nil) {
            NSLog(@"JSON Parsing error !!!");
            dispatch_async(dispatch_get_main_queue(), ^{
                handlar(NO, error, nil);
            });
        }
        else{
            NSLog(@"Some error !!!");
            dispatch_async(dispatch_get_main_queue(), ^{
                handlar(NO, connectionError, nil);
            });
        }
    }];
    [dataTask resume];
}



+(void)GetDataForMethod:(NSString *)serviceName withParameters:(NSDictionary *)param withFiles:(NSArray *)files onCompletion:(CompletionHandler)handlar{

    NSString *urlString= [NSString stringWithFormat:@"%@/%@",[BASEURL stringByDeletingLastPathComponent] ,serviceName];
    NSLog(@"Request URL : %@",urlString);
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setURL:[NSURL URLWithString:urlString]];
    [request setHTTPMethod:@"POST"];
    request.timeoutInterval = 300.0;
    [request addValue:@"8bit" forHTTPHeaderField:@"Content-Transfer-Encoding"];
    
    NSMutableData *body = [NSMutableData data];
    
    NSString *boundary = @"---------------------------14737809831466499882746641449";
    NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@", boundary];
    [request addValue:contentType forHTTPHeaderField:@"Content-Type"];
    
    
    
    for (id key in param) {
        
        [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"\r\n\r\n", key] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"%@",[param objectForKey:key]] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
        
    }
    
    for (int i=0; i<[files count]; i++) {
        
        [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"uploadedfile_%d.png\"; filename=\"uploadedfile_%d.png\"\r\n\r\n",i,i] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[NSData dataWithData:[files objectAtIndex:i]]];
        [body appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
        
    }
    
    [body appendData:[[NSString stringWithFormat:@"--%@--\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [request setHTTPBody:body];
    
    NSString *strData = [[NSString alloc] initWithData:body encoding:NSASCIIStringEncoding];
    NSLog(@"Input Packet : %@",strData);
    
    
    NSOperationQueue *queue = [[NSOperationQueue alloc] init];
    [NSURLConnection sendAsynchronousRequest:request queue:queue completionHandler:^(NSURLResponse *response, NSData *data, NSError *connectionError) {
    
        if(data == nil){
            dispatch_async(dispatch_get_main_queue(), ^{
                handlar(NO, nil, @{@"ResponseMessage" : connectionError.userInfo[@"NSLocalizedDescription"]});
            });
            return;
        }
        
        NSString *strData = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
        NSLog(@"Got Response : %@",strData);
        
        NSError *error;
        NSDictionary *jsonResult = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&error];
        if (jsonResult != nil){
            
            if([[jsonResult objectForKey:@"responsecode"] integerValue] == 200){
                //Success
                dispatch_async(dispatch_get_main_queue(), ^{
                    handlar(YES, nil, jsonResult);
                });
                
            }
            else{
                //faliour
                dispatch_async(dispatch_get_main_queue(), ^{
                    handlar(NO, nil, jsonResult);
                });
                
            }
        }
        else if (error != nil) {
            NSLog(@"JSON Parsing error !!!");
            dispatch_async(dispatch_get_main_queue(), ^{
                handlar(NO, error, nil);
            });
        }
        else{
            NSLog(@"Some error !!!");
            dispatch_async(dispatch_get_main_queue(), ^{
                handlar(NO, connectionError, nil);
            });
        }
        
    }];
}

@end
