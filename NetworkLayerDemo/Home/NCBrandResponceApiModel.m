//
//  NCBrandResponceApiModel.m
//  OneAppMD
//  Copyright © 2016 Girnar. All rights reserved.
//

#import "NCBrandResponceApiModel.h"

@implementation NCBrand

//- (NSDictionary*)jsonMapping {
//    return @{
//             @"id"          : @"brandId"
//             };
//}

@end

@implementation NCBrandResponceApiModel

- (NSDictionary*)jsonClasses {
    return @{
             @"data" : [NCBrand class]
             };
}

@end
