//
//  CDHomeApiMapper.h
//  NetworkLayer
//
//  Created by Vasim Akram on 5/30/17.
//  Copyright © 2017 VS. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CDHomeAuthor : NSObject

@property(assign, nonatomic) NSInteger      authorId;
@property(strong, nonatomic) NSString       *name;
@property(strong, nonatomic) NSString       *email;
@property(strong, nonatomic) NSString       *slug;
@property(strong, nonatomic) NSString       *authorDescription;

@end

@interface CDHomeExpertReview : NSObject

@property(assign, nonatomic) NSInteger      reviewId;
@property(strong, nonatomic) NSString       *title;
@property(strong, nonatomic) NSString       *slug;
@property(strong, nonatomic) NSString       *brand;
@property(strong, nonatomic) NSString       *model;
@property(assign, nonatomic) NSInteger      viewCount;
@property(strong, nonatomic) NSString       *content;
@property(strong, nonatomic) NSString       *publishedAt;
@property(strong, nonatomic) NSString       *coverImage;
@property(strong, nonatomic) NSString       *thumbnail;
@property(strong, nonatomic) CDHomeAuthor       *author;

@end


@interface CDHomeServices : NSObject

@property(strong, nonatomic) NSString       *name;
@property(strong, nonatomic) NSString       *link;
@property(strong, nonatomic) NSString       *slug;

@end


@interface CDHomeNews : NSObject

@property(assign, nonatomic) NSInteger      newsId;//
@property(strong, nonatomic) NSString       *title;
@property(strong, nonatomic) NSString       *slug;
@property(strong, nonatomic) NSString       *brand;
@property(strong, nonatomic) NSString       *model;
@property(assign, nonatomic) NSInteger      viewCount;
@property(strong, nonatomic) NSString       *content;
@property(strong, nonatomic) NSString       *publishedAt;
@property(strong, nonatomic) NSString       *coverImage;
@property(strong, nonatomic) NSString       *thumbnail;
@property(strong, nonatomic) NSString       *url;
@property(strong, nonatomic) CDHomeAuthor       *author;

@end


@interface CDHomeVideos : NSObject

@property(assign, nonatomic) NSInteger      videoKeyId;
@property(strong, nonatomic) NSString       *title;
@property(strong, nonatomic) NSString       *slug;
@property(strong, nonatomic) NSString       *videoId;
@property(assign, nonatomic) NSInteger      count;
@property(strong, nonatomic) NSString       *duration;
@property(strong, nonatomic) NSString       *videoDescription;
@property(strong, nonatomic) NSString       *publishedAt;

@end

@interface CDHomeFeturedStories : NSObject

@property(assign, nonatomic) NSInteger      storyId;
@property(strong, nonatomic) NSString       *title;
@property(strong, nonatomic) NSString       *slug;
@property(strong, nonatomic) NSString       *brand;
@property(strong, nonatomic) NSString       *model;
@property(assign, nonatomic) NSInteger      viewCount;
@property(strong, nonatomic) NSString       *content;
@property(strong, nonatomic) NSString       *publishedAt;
@property(strong, nonatomic) NSString       *coverImage;
@property(strong, nonatomic) NSString       *thumbnail;
@property(strong, nonatomic) NSString       *url;
@property(strong, nonatomic) CDHomeAuthor       *author;

@end

@interface CDHomeCarKeyFeture : NSObject

@property(strong, nonatomic) NSString       *name;
@property(strong, nonatomic) NSString       *slug;
@property(strong, nonatomic) NSString       *value;
@property(strong, nonatomic) NSString       *unit;
@property(assign, nonatomic) BOOL           isKeyFeature;
@property(strong, nonatomic) NSString       *groupName;
@property(strong, nonatomic) NSString       *category;

@end

@interface CDHomeCarDiscription : NSObject

@property(assign, nonatomic) NSInteger      carId;
@property(strong, nonatomic) NSString       *brandName;
@property(strong, nonatomic) NSString       *brandSlug;
@property(strong, nonatomic) NSString       *name;
@property(strong, nonatomic) NSString       *slug;
@property(assign, nonatomic) double         avgRating;
@property(assign, nonatomic) NSInteger      reviewCount;
@property(strong, nonatomic) NSString       *variantName;
@property(strong, nonatomic) NSString       *variantSlug;
@property(strong, nonatomic) NSString       *variantPrice;
@property(strong, nonatomic) NSString       *minPrice;
@property(strong, nonatomic) NSString       *maxPrice;
@property(strong, nonatomic) NSString       *priceRange;
@property(strong, nonatomic) NSString       *image;
@property(strong, nonatomic) NSString       *status;
@property(strong, nonatomic) NSString       *highlights;
@property(strong, nonatomic) NSString       *carDescription;
@property(strong, nonatomic) NSString       *launchedAt;
@property(strong, nonatomic) NSString       *expiredAt;
@property(assign, nonatomic) BOOL           isSponsored;
@property(strong, nonatomic) NSArray        *keyFeatures;

@end


@interface CDHomeFeturedCar : NSObject

@property(strong, nonatomic) NSArray       *latest;
@property(strong, nonatomic) NSArray       *upcoming;
@property(strong, nonatomic) NSArray       *popular;

@end


@interface CDHomeTrendingComprarison : NSObject

@property(assign, nonatomic) NSInteger      carId;//
@property(strong, nonatomic) NSString       *brandName;
@property(strong, nonatomic) NSString       *brandSlug;
@property(strong, nonatomic) NSString       *name;
@property(strong, nonatomic) NSString       *slug;
@property(assign, nonatomic) double         avgRating;
@property(assign, nonatomic) NSInteger      reviewCount;
@property(strong, nonatomic) NSString       *variantName;
@property(strong, nonatomic) NSString       *variantSlug;
@property(strong, nonatomic) NSString       *variantPrice;
@property(strong, nonatomic) NSString       *minPrice;
@property(strong, nonatomic) NSString       *maxPrice;
@property(strong, nonatomic) NSString       *priceRange;
@property(strong, nonatomic) NSString       *image;
@property(strong, nonatomic) NSString       *status;
@property(strong, nonatomic) NSString       *highlights;
@property(strong, nonatomic) NSString       *carDescription;//
@property(strong, nonatomic) NSString       *launchedAt;
@property(strong, nonatomic) NSString       *expiredAt;
@property(assign, nonatomic) BOOL           isSponsored;
@property(strong, nonatomic) NSArray        *keyFeatures;

@end


@interface CDUModel : NSObject

@property(assign, nonatomic) NSInteger      brandId;
@property(strong, nonatomic) NSString       *brand;
@property(strong, nonatomic) NSString       *brandSlug;
@property(assign, nonatomic) NSInteger      modelId;
@property(strong, nonatomic) NSString       *model;
@property(strong, nonatomic) NSString       *modelSlug;
@property(strong, nonatomic) NSString       *image;
@property(strong, nonatomic) NSNumber       *minPrice;
@property(strong, nonatomic) NSNumber       *maxPrice;
@property(strong, nonatomic) NSString       *displayMinPrice;
@property(strong, nonatomic) NSString       *score;
@property(strong, nonatomic) NSNumber       *usedCarCount;

@end


@interface CDUFilter : NSObject

@property(strong, nonatomic) NSString       *name;
@property(strong, nonatomic) NSString       *slug;
@property(strong, nonatomic) NSNumber       *minPrice;
@property(strong, nonatomic) NSNumber       *maxPrice;
@property(strong, nonatomic) NSString       *displayMinPrice;
@property(strong, nonatomic) NSString       *displayMaxPrice;
@property(strong, nonatomic) NSArray       *models;

@end

@interface CDUFilterGroup : NSObject

@property(strong, nonatomic) NSString       *name;
@property(strong, nonatomic) NSString       *slug;
@property(strong, nonatomic) NSArray       *filters;

@end

@interface CDUsedCar : NSObject

@property(strong, nonatomic) NSArray       *filterGroups;

@end


@interface CDHomeApiData : NSObject

@property(strong, nonatomic) NSArray       *expertReviews;
@property(strong, nonatomic) NSArray       *services;
@property(strong, nonatomic) NSArray       *news;
@property(strong, nonatomic) NSArray       *videos;
@property(strong, nonatomic) NSArray       *featuredStories;
@property(strong, nonatomic) CDHomeFeturedCar *featuredCars;
@property(strong, nonatomic) NSArray       *trendingComparison;
@property(strong, nonatomic) CDUsedCar       *usedCars;

@end


@interface CDHomeApiMapper : NSObject

@property(assign, nonatomic) NSInteger      statusCode;
@property(assign, nonatomic) BOOL           status;
@property(strong, nonatomic) NSNumber       *statusText;
@property(strong, nonatomic) CDHomeApiData   *data;

@end
