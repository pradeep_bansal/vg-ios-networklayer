//
//  NCBrandResponceApiModel.h
//  OneAppMD
//
//  Copyright © 2016 Girnar. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BaseResponse.h"
@interface NCBrand : NSObject
@property(assign, nonatomic) NSInteger          brandId;
@property(strong, nonatomic) NSString           *name;
@property(strong, nonatomic) NSString           *slug;
@property(strong, nonatomic) NSString           *image;
@property(assign, nonatomic) BOOL          isPopular;
@property(assign, nonatomic) NSInteger          popularity;
@end

@interface NCBrandResponceApiModel : BaseResponse
@property(strong, nonatomic) NSMutableArray<NCBrand*> *data;
@end
