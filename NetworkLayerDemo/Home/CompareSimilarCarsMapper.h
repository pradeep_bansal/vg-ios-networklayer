//
//  CompareSimilarCarsMapper.h
//  Gaadi
//
//  Created by VS-Saddam Husain-MacBookPro on 14/10/16.
//  Copyright © 2016 VS. All rights reserved.
//

#import <Foundation/Foundation.h>

@class  CompareSimilarCarsDataMapper;
@class  CompareSimilarCarsVariantMapper;
@class  CompareSimilarCarsKeyFeatureMapper;

@interface CompareSimilarCarsMapper : NSObject

@property(strong, nonatomic) NSString *status;
@property(strong, nonatomic) NSString *statusCode;
@property(strong, nonatomic) NSString *statusText;
@property(strong, nonatomic) NSArray<CompareSimilarCarsDataMapper *> *data;

@end


@interface CompareSimilarCarsDataMapper : NSObject

@property (strong, nonatomic) NSString *modelId;
@property (strong, nonatomic) NSString *brand;
@property (strong, nonatomic) NSString *brandSlug;
@property (strong, nonatomic) NSString *model;
@property (strong, nonatomic) NSString *modelSlug;
@property (strong, nonatomic) NSString *status;
@property (strong, nonatomic) NSString *image;
@property (strong, nonatomic) NSString *maxPrice;
@property (strong, nonatomic) NSString *minPrice;
@property (strong, nonatomic) NSString *priceRange;
@property (strong, nonatomic) NSString *score;
@property (strong, nonatomic) NSString *avgRating;
@property (strong, nonatomic) NSString *reviewCount;
@property (strong, nonatomic) NSString *is360;
@property (strong, nonatomic) NSString *isFtc;

@property (strong, nonatomic) NSArray<CompareSimilarCarsVariantMapper *> *variants;
@property (strong, nonatomic) NSArray<CompareSimilarCarsKeyFeatureMapper *> *keyFeatures;
@property (strong, nonatomic) NSArray *colors;
@property (strong, nonatomic) NSArray *videos;
@property (strong, nonatomic) NSArray *images;
@property (strong, nonatomic) NSArray *similarCars;

@end



@interface CompareSimilarCarsVariantMapper : NSObject

@property (strong, nonatomic) NSString *variantId;
@property (strong, nonatomic) NSString *name;
@property (strong, nonatomic) NSString *slug;
@property (strong, nonatomic) NSString *status;
@property (strong, nonatomic) NSString *vehicleType;
@property (strong, nonatomic) NSString *fuelType;
@property (strong, nonatomic) NSString *price;
@property (strong, nonatomic) NSString *launchedAt;
@property (strong, nonatomic) NSString *expiredAt;
@property (strong, nonatomic) NSString *createdAt;
@property (strong, nonatomic) NSString *updatedAt;
@property (strong, nonatomic) NSArray<CompareSimilarCarsKeyFeatureMapper *> *keyFeatures;

@end



@interface CompareSimilarCarsKeyFeatureMapper : NSObject


@property (strong, nonatomic) NSString *keyId;
@property (strong, nonatomic) NSString *name;
@property (strong, nonatomic) NSString *slug;
@property (strong, nonatomic) NSString *value;
@property (strong, nonatomic) NSString *unit;
@property (strong, nonatomic) NSString *keyDescription;
@property (strong, nonatomic) NSString *priority;
@property (strong, nonatomic) NSString *isKeyFeature;
@property (strong, nonatomic) NSString *groupName;
@property (strong, nonatomic) NSString *category;


@end

