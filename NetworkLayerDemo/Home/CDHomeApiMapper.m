//
//  CDHomeApiMapper.m
//  NetworkLayer
//
//  Created by Vasim Akram on 5/30/17.
//  Copyright © 2017 VS. All rights reserved.
//

#import "CDHomeApiMapper.h"

@implementation CDHomeAuthor : NSObject

-(NSDictionary *)jsonMapping{
    return @{
             @"id" : @"authorId",
             @"description" : @"authorDescription"
             };
}

@end

@implementation CDHomeExpertReview : NSObject

-(NSDictionary *)jsonMapping{
    return @{
             @"id" : @"reviewId",
             };
}

@end


@implementation CDHomeServices : NSObject

@end


@implementation CDHomeNews : NSObject

-(NSDictionary *)jsonMapping{
    return @{
             @"id" : @"newsId",
             };
}

@end


@implementation CDHomeVideos : NSObject

-(NSDictionary *)jsonMapping{
    return @{
             @"id" : @"videoKeyId",
             @"description" : @"videoDescription"
             };
}

@end

@implementation CDHomeFeturedStories : NSObject

-(NSDictionary *)jsonMapping{
    return @{
             @"id" : @"storyId",
             };
}

@end

@implementation CDHomeCarKeyFeture : NSObject

@end

@implementation CDHomeCarDiscription : NSObject

-(NSDictionary *)jsonMapping{
    return @{
             @"id" : @"carId",
             @"description" : @"carDescription"
             };
}
-(NSDictionary *)jsonClasses{
    return @{
             @"keyFeatures" : [CDHomeCarKeyFeture class]
             };
}

@end


@implementation CDHomeFeturedCar : NSObject

-(NSDictionary *)jsonClasses{
    return @{
             @"latest" : [CDHomeCarDiscription class],
             @"upcoming" : [CDHomeCarDiscription class],
             @"popular" : [CDHomeCarDiscription class]
             };
}

@end


@implementation CDHomeTrendingComprarison : NSObject

-(NSDictionary *)jsonMapping{
    return @{
             @"id" : @"carId",
             @"description" : @"carDescription"
             };
}
-(NSDictionary *)jsonClasses{
    return @{
             @"keyFeatures" : [CDHomeCarKeyFeture class]
             };
}

@end


@implementation CDUModel : NSObject

@end


@implementation CDUFilter : NSObject

-(NSDictionary *)jsonClasses{
    return @{
             @"models" : [CDUModel class]
             };
}

@end

@implementation CDUFilterGroup : NSObject

-(NSDictionary *)jsonClasses{
    return @{
             @"filters" : [CDUFilter class]
             };
}

@end

@implementation CDUsedCar : NSObject

-(NSDictionary *)jsonClasses{
    return @{
             @"filterGroups" : [CDUFilterGroup class]
             };
}

@end

@implementation CDHomeApiData : NSObject

-(NSDictionary *)jsonClasses{
    return @{
             @"expertReviews" : [CDHomeExpertReview class],
             @"services" : [CDHomeServices class],
             @"news" : [CDHomeNews class],
             @"videos" : [CDHomeVideos class],
             @"featuredStories" : [CDHomeFeturedStories class],
             };
}

@end

@implementation CDHomeApiMapper
//- (NSDictionary*)jsonClasses {
//    return @{
//             @"data" : [CDHomeApiData class]
//             };
//}
+ (BOOL)printLogs {
    return YES;
}

@end
