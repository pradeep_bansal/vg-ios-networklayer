//
//  BaseResponse.h
//  NetworkLayer
//
//  Created by Deepak on 22/09/17.
//  Copyright © 2017 VS. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BaseResponse : NSObject
@property(assign, nonatomic) NSInteger      statusCode;
@property(assign, nonatomic) BOOL           status;
@property(strong, nonatomic) NSString       *statusText;
@end
