//
//  CompareSimilarCarsMapper.m
//  Gaadi
//
//  Created by VS-Saddam Husain-MacBookPro on 14/10/16.
//  Copyright © 2016 VS. All rights reserved.
//

#import "CompareSimilarCarsMapper.h"
#define kCompareSimilarCarsMapper @"CompareSimilarCarsMapper"

@implementation CompareSimilarCarsMapper


+ (NSString *)fileNameForParamDict:(NSDictionary *)dictionary {
    
    return [NSString stringWithFormat:@"%@_%@_%@.json", kCompareSimilarCarsMapper, [dictionary objectForKey:@"brandSlug"],[dictionary objectForKey:@"modelSlug"]];

}




+ (BOOL)allowCaching {
    
    return YES;
}




- (NSDictionary*)jsonClasses {
    return @{
             @"data" : [CompareSimilarCarsDataMapper class]
             };
}




@end


@implementation CompareSimilarCarsDataMapper


- (NSDictionary*)jsonClasses {
    return @{
             @"keyFeatures" : [CompareSimilarCarsKeyFeatureMapper class],
             @"variants" : [CompareSimilarCarsVariantMapper class],
             @"colors" : [NSArray class],
             @"videos" : [NSArray class],
             @"images" : [NSArray class],
             @"similarCars" : [NSArray class]

             };
}



- (NSDictionary*)jsonMapping {
    return @{
             @"id": @"modelId"
             };
}



@end


@implementation CompareSimilarCarsVariantMapper

- (NSDictionary*)jsonClasses {
    return @{
             @"keyFeatures" : [CompareSimilarCarsKeyFeatureMapper class]
             };
}



- (NSDictionary*)jsonMapping {
    return @{
             @"id": @"variantId"
             };
}


@end

@implementation CompareSimilarCarsKeyFeatureMapper


- (NSDictionary*)jsonMapping {
    return @{
             @"id": @"keyId",
             @"description": @"keyDescription"
             
             };
}



@end
