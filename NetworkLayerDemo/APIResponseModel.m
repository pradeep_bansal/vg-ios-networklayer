//
//  APIResponseModel.m
//  NetworkLayer
//
//  Created by Vasim Akram on 4/24/17.
//  Copyright © 2017 VS. All rights reserved.
//

#import "APIResponseModel.h"


NSString *const kBaseClassStatus = @"status";
NSString *const kBaseClassStatusCode = @"statusCode";
NSString *const kBaseClassStatusText = @"statusText";
NSString *const kBaseClassData = @"data";

@implementation APIResponseModel


//+ (BOOL)allowCaching {
//    return NO;
//}


- (instancetype)initWithDictionary:(NSDictionary *)dict {
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if (self && [dict isKindOfClass:[NSDictionary class]]) {
        self.status = [[self objectOrNilForKey:kBaseClassStatus fromDictionary:dict] boolValue];
        self.statusCode = [[self objectOrNilForKey:kBaseClassStatusCode fromDictionary:dict] doubleValue];
        self.statusText = [self objectOrNilForKey:kBaseClassStatusText fromDictionary:dict];
        self.data = [self objectOrNilForKey:kBaseClassData fromDictionary:dict];
        
    }
    
    return self;
    
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict {
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}

+ (BOOL)printLogs {
    return YES;
}

@end
