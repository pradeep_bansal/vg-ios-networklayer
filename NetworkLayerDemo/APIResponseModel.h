//
//  APIResponseModel.h
//  NetworkLayer
//
//  Created by Vasim Akram on 4/24/17.
//  Copyright © 2017 VS. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface APIResponseModel : NSObject

@property(assign, nonatomic) NSInteger      statusCode;
@property(assign, nonatomic) BOOL           status;
@property(strong, nonatomic) NSNumber       *statusText;
@property(strong, nonatomic) id   data;

@end
