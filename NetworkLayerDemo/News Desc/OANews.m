//
//  OANews.m
//
//  Created by Sourabh Goyal on 3/24/17
//  Copyright (c) 2017 __MyCompanyName__. All rights reserved.
//

#import "OANews.h"
#import "OAAuthor.h"


NSString *const kOANewsId = @"id";
NSString *const kOANewsCoverImage = @"coverImage";
NSString *const kOANewsAuthor = @"author";
NSString *const kOANewsBrand = @"brand";
NSString *const kOANewsPublishedAt = @"publishedAt";
NSString *const kOANewsUrl = @"url";
NSString *const kOANewsTitle = @"title";
NSString *const kOANewsViewCount = @"viewCount";
NSString *const kOANewsSlug = @"slug";
NSString *const kOANewsThumbnail = @"thumbnail";
NSString *const kOANewsModel = @"model";
NSString *const kOANewsContent = @"content";
NSString *const kOANewsGallery = @"gallery";


@interface OANews ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation OANews

@synthesize newsIdentifier = _newsIdentifier;
@synthesize coverImage = _coverImage;
@synthesize author = _author;
@synthesize brand = _brand;
@synthesize publishedAt = _publishedAt;
@synthesize url = _url;
@synthesize title = _title;
@synthesize viewCount = _viewCount;
@synthesize slug = _slug;
@synthesize thumbnail = _thumbnail;
@synthesize model = _model;
@synthesize content = _content;
@synthesize gallery = _gallery;



+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict
{
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if(self && [dict isKindOfClass:[NSDictionary class]]) {
            self.newsIdentifier = [[self objectOrNilForKey:kOANewsId fromDictionary:dict] doubleValue];
            self.coverImage = [self objectOrNilForKey:kOANewsCoverImage fromDictionary:dict];
            self.author = [OAAuthor modelObjectWithDictionary:[dict objectForKey:kOANewsAuthor]];
            self.brand = [self objectOrNilForKey:kOANewsBrand fromDictionary:dict];
            self.publishedAt = [self objectOrNilForKey:kOANewsPublishedAt fromDictionary:dict];
            self.url = [self objectOrNilForKey:kOANewsUrl fromDictionary:dict];
            self.title = [self objectOrNilForKey:kOANewsTitle fromDictionary:dict];
            self.viewCount = [[self objectOrNilForKey:kOANewsViewCount fromDictionary:dict] doubleValue];
            self.slug = [self objectOrNilForKey:kOANewsSlug fromDictionary:dict];
            self.thumbnail = [self objectOrNilForKey:kOANewsThumbnail fromDictionary:dict];
            self.model = [self objectOrNilForKey:kOANewsModel fromDictionary:dict];
            self.content = [self objectOrNilForKey:kOANewsContent fromDictionary:dict];
          self.gallery = [self objectOrNilForKey:kOANewsGallery fromDictionary:dict];

    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation
{
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:[NSNumber numberWithDouble:self.newsIdentifier] forKey:kOANewsId];
   
    [mutableDict setValue:[self.author dictionaryRepresentation] forKey:kOANewsAuthor];
    [mutableDict setValue:self.brand forKey:kOANewsBrand];
    [mutableDict setValue:self.publishedAt forKey:kOANewsPublishedAt];
    [mutableDict setValue:self.url forKey:kOANewsUrl];
    [mutableDict setValue:self.title forKey:kOANewsTitle];
    [mutableDict setValue:[NSNumber numberWithDouble:self.viewCount] forKey:kOANewsViewCount];
    [mutableDict setValue:self.slug forKey:kOANewsSlug];
    [mutableDict setValue:self.thumbnail forKey:kOANewsThumbnail];
    [mutableDict setValue:self.model forKey:kOANewsModel];
    [mutableDict setValue:self.content forKey:kOANewsContent];
    [mutableDict setValue:self.coverImage forKey:kOANewsCoverImage];
    [mutableDict setValue:self.gallery forKey:kOANewsGallery];

    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description 
{
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict
{
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];

    self.newsIdentifier = [aDecoder decodeDoubleForKey:kOANewsId];
    self.coverImage = [aDecoder decodeObjectForKey:kOANewsCoverImage];
    self.author = [aDecoder decodeObjectForKey:kOANewsAuthor];
    self.brand = [aDecoder decodeObjectForKey:kOANewsBrand];
    self.publishedAt = [aDecoder decodeObjectForKey:kOANewsPublishedAt];
    self.url = [aDecoder decodeObjectForKey:kOANewsUrl];
    self.title = [aDecoder decodeObjectForKey:kOANewsTitle];
    self.viewCount = [aDecoder decodeDoubleForKey:kOANewsViewCount];
    self.slug = [aDecoder decodeObjectForKey:kOANewsSlug];
    self.thumbnail = [aDecoder decodeObjectForKey:kOANewsThumbnail];
    self.model = [aDecoder decodeObjectForKey:kOANewsModel];
    self.content = [aDecoder decodeObjectForKey:kOANewsContent];
    self.gallery = [aDecoder decodeObjectForKey:kOANewsGallery];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{

    [aCoder encodeDouble:_newsIdentifier forKey:kOANewsId];
    [aCoder encodeObject:_coverImage forKey:kOANewsCoverImage];
    [aCoder encodeObject:_author forKey:kOANewsAuthor];
    [aCoder encodeObject:_brand forKey:kOANewsBrand];
    [aCoder encodeObject:_publishedAt forKey:kOANewsPublishedAt];
    [aCoder encodeObject:_url forKey:kOANewsUrl];
    [aCoder encodeObject:_title forKey:kOANewsTitle];
    [aCoder encodeDouble:_viewCount forKey:kOANewsViewCount];
    [aCoder encodeObject:_slug forKey:kOANewsSlug];
    [aCoder encodeObject:_thumbnail forKey:kOANewsThumbnail];
    [aCoder encodeObject:_model forKey:kOANewsModel];
    [aCoder encodeObject:_content forKey:kOANewsContent];
    [aCoder encodeObject:_content forKey:kOANewsGallery];
}

- (id)copyWithZone:(NSZone *)zone
{
    OANews *copy = [[OANews alloc] init];
    
    if (copy) {

        copy.newsIdentifier = self.newsIdentifier;
        copy.coverImage = [self.coverImage copyWithZone:zone];
        copy.author = [self.author copyWithZone:zone];
        copy.brand = [self.brand copyWithZone:zone];
        copy.publishedAt = [self.publishedAt copyWithZone:zone];
        copy.url = [self.url copyWithZone:zone];
        copy.title = [self.title copyWithZone:zone];
        copy.viewCount = self.viewCount;
        copy.slug = [self.slug copyWithZone:zone];
        copy.thumbnail = [self.thumbnail copyWithZone:zone];
        copy.model = [self.model copyWithZone:zone];
        copy.content = [self.content copyWithZone:zone];
        copy.gallery = [self.gallery copyWithZone:zone];
    }
    
    return copy;
}


@end
