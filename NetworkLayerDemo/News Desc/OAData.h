//
//  OAData.h
//
//  Created by Sourabh Goyal on 3/24/17
//  Copyright (c) 2017 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@class OANews,OARelatedNews;

@interface OAData : NSObject <NSCoding, NSCopying>

@property (nonatomic, strong) OANews *news;
@property (nonatomic, strong) NSArray<OARelatedNews*> *relatedNews;

+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict;
- (instancetype)initWithDictionary:(NSDictionary *)dict;
- (NSDictionary *)dictionaryRepresentation;

@end
