//
//  OAData.m
//
//  Created by Sourabh Goyal on 3/24/17
//  Copyright (c) 2017 __MyCompanyName__. All rights reserved.
//

#import "OAData.h"
#import "OANews.h"
#import "OARelatedNews.h"


NSString *const kOADataNews = @"news";
NSString *const kOADataRelatedNews = @"relatedNews";


@interface OAData ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation OAData

@synthesize news = _news;
@synthesize relatedNews = _relatedNews;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict
{
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if(self && [dict isKindOfClass:[NSDictionary class]]) {
            self.news = [OANews modelObjectWithDictionary:[dict objectForKey:kOADataNews]];
    NSObject *receivedOARelatedNews = [dict objectForKey:kOADataRelatedNews];
    NSMutableArray *parsedOARelatedNews = [NSMutableArray array];
    if ([receivedOARelatedNews isKindOfClass:[NSArray class]]) {
        for (NSDictionary *item in (NSArray *)receivedOARelatedNews) {
            if ([item isKindOfClass:[NSDictionary class]]) {
                [parsedOARelatedNews addObject:[OARelatedNews modelObjectWithDictionary:item]];
            }
       }
    } else if ([receivedOARelatedNews isKindOfClass:[NSDictionary class]]) {
       [parsedOARelatedNews addObject:[OARelatedNews modelObjectWithDictionary:(NSDictionary *)receivedOARelatedNews]];
    }

    self.relatedNews = [NSArray arrayWithArray:parsedOARelatedNews];

    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation
{
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:[self.news dictionaryRepresentation] forKey:kOADataNews];
    NSMutableArray *tempArrayForRelatedNews = [NSMutableArray array];
    for (NSObject *subArrayObject in self.relatedNews) {
        if([subArrayObject respondsToSelector:@selector(dictionaryRepresentation)]) {
            // This class is a model object
            [tempArrayForRelatedNews addObject:[subArrayObject performSelector:@selector(dictionaryRepresentation)]];
        } else {
            // Generic object
            [tempArrayForRelatedNews addObject:subArrayObject];
        }
    }
    [mutableDict setValue:[NSArray arrayWithArray:tempArrayForRelatedNews] forKey:kOADataRelatedNews];

    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description 
{
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict
{
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];

    self.news = [aDecoder decodeObjectForKey:kOADataNews];
    self.relatedNews = [aDecoder decodeObjectForKey:kOADataRelatedNews];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{

    [aCoder encodeObject:_news forKey:kOADataNews];
    [aCoder encodeObject:_relatedNews forKey:kOADataRelatedNews];
}

- (id)copyWithZone:(NSZone *)zone
{
    OAData *copy = [[OAData alloc] init];
    
    if (copy) {

        copy.news = [self.news copyWithZone:zone];
        copy.relatedNews = [self.relatedNews copyWithZone:zone];
    }
    
    return copy;
}

- (NSDictionary*)jsonClasses {
    return @{
             @"relatedNews" : [OARelatedNews class]
             };
}

@end
