//
//  OANews.h
//
//  Created by Sourabh Goyal on 3/24/17
//  Copyright (c) 2017 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@class OAAuthor;

@interface OANews : NSObject <NSCoding, NSCopying>

@property (nonatomic, assign) double newsIdentifier;
@property (nonatomic, strong) NSString *coverImage;
@property (nonatomic, strong) OAAuthor *author;
@property (nonatomic, strong) NSString *brand;
@property (nonatomic, strong) NSString *publishedAt;
@property (nonatomic, strong) NSString *url;
@property (nonatomic, strong) NSString *title;
@property (nonatomic, assign) double viewCount;
@property (nonatomic, strong) NSString *slug;
@property (nonatomic, strong) NSString *thumbnail;
@property (nonatomic, strong) NSString *model;
@property (nonatomic, strong) NSString *content;
@property (nonatomic, strong) NSArray *gallery;

+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict;
- (instancetype)initWithDictionary:(NSDictionary *)dict;
- (NSDictionary *)dictionaryRepresentation;

@end
