//
//  OANewsDetailModel.m
//
//  Created by Sourabh Goyal on 3/24/17
//  Copyright (c) 2017 __MyCompanyName__. All rights reserved.
//

#import "OANewsDetailModel.h"
#import "OAData.h"


NSString *const kOANewsDetailModelStatus = @"status";
NSString *const kOANewsDetailModelStatusCode = @"statusCode";
NSString *const kOANewsDetailModelStatusText = @"statusText";
NSString *const kOANewsDetailModelData = @"data";


@interface OANewsDetailModel ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation OANewsDetailModel

@synthesize status = _status;
@synthesize statusCode = _statusCode;
@synthesize statusText = _statusText;
@synthesize data = _data;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict
{
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if(self && [dict isKindOfClass:[NSDictionary class]]) {
            self.status = [[self objectOrNilForKey:kOANewsDetailModelStatus fromDictionary:dict] boolValue];
            self.statusCode = [[self objectOrNilForKey:kOANewsDetailModelStatusCode fromDictionary:dict] doubleValue];
            self.statusText = [self objectOrNilForKey:kOANewsDetailModelStatusText fromDictionary:dict];
            self.data = [OAData modelObjectWithDictionary:[dict objectForKey:kOANewsDetailModelData]];

    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation
{
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:[NSNumber numberWithBool:self.status] forKey:kOANewsDetailModelStatus];
    [mutableDict setValue:[NSNumber numberWithDouble:self.statusCode] forKey:kOANewsDetailModelStatusCode];
    [mutableDict setValue:self.statusText forKey:kOANewsDetailModelStatusText];
    [mutableDict setValue:[self.data dictionaryRepresentation] forKey:kOANewsDetailModelData];

    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description 
{
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict
{
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];

    self.status = [aDecoder decodeBoolForKey:kOANewsDetailModelStatus];
    self.statusCode = [aDecoder decodeDoubleForKey:kOANewsDetailModelStatusCode];
    self.statusText = [aDecoder decodeObjectForKey:kOANewsDetailModelStatusText];
    self.data = [aDecoder decodeObjectForKey:kOANewsDetailModelData];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{

    [aCoder encodeBool:_status forKey:kOANewsDetailModelStatus];
    [aCoder encodeDouble:_statusCode forKey:kOANewsDetailModelStatusCode];
    [aCoder encodeObject:_statusText forKey:kOANewsDetailModelStatusText];
    [aCoder encodeObject:_data forKey:kOANewsDetailModelData];
}

- (id)copyWithZone:(NSZone *)zone
{
    OANewsDetailModel *copy = [[OANewsDetailModel alloc] init];
    
    if (copy) {

        copy.status = self.status;
        copy.statusCode = self.statusCode;
        copy.statusText = [self.statusText copyWithZone:zone];
        copy.data = [self.data copyWithZone:zone];
    }
    
    return copy;
}


@end
