//
//  OAAuthor.m
//
//  Created by Sourabh Goyal on 3/24/17
//  Copyright (c) 2017 __MyCompanyName__. All rights reserved.
//

#import "OAAuthor.h"


NSString *const kOAAuthorEmail = @"email";
NSString *const kOAAuthorId = @"id";
NSString *const kOAAuthorSlug = @"slug";
NSString *const kOAAuthorName = @"name";
NSString *const kOAAuthorDescription = @"description";


@interface OAAuthor ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation OAAuthor

@synthesize email = _email;
@synthesize authorIdentifier = _authorIdentifier;
@synthesize slug = _slug;
@synthesize name = _name;
@synthesize authorDescription = _authorDescription;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict
{
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if(self && [dict isKindOfClass:[NSDictionary class]]) {
            self.email = [self objectOrNilForKey:kOAAuthorEmail fromDictionary:dict];
            self.authorIdentifier = [[self objectOrNilForKey:kOAAuthorId fromDictionary:dict] doubleValue];
            self.slug = [self objectOrNilForKey:kOAAuthorSlug fromDictionary:dict];
            self.name = [self objectOrNilForKey:kOAAuthorName fromDictionary:dict];
            self.authorDescription = [self objectOrNilForKey:kOAAuthorDescription fromDictionary:dict];

    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation
{
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:self.email forKey:kOAAuthorEmail];
    [mutableDict setValue:[NSNumber numberWithDouble:self.authorIdentifier] forKey:kOAAuthorId];
    [mutableDict setValue:self.slug forKey:kOAAuthorSlug];
    [mutableDict setValue:self.name forKey:kOAAuthorName];
    [mutableDict setValue:self.authorDescription forKey:kOAAuthorDescription];

    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description 
{
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict
{
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];

    self.email = [aDecoder decodeObjectForKey:kOAAuthorEmail];
    self.authorIdentifier = [aDecoder decodeDoubleForKey:kOAAuthorId];
    self.slug = [aDecoder decodeObjectForKey:kOAAuthorSlug];
    self.name = [aDecoder decodeObjectForKey:kOAAuthorName];
    self.authorDescription = [aDecoder decodeObjectForKey:kOAAuthorDescription];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{

    [aCoder encodeObject:_email forKey:kOAAuthorEmail];
    [aCoder encodeDouble:_authorIdentifier forKey:kOAAuthorId];
    [aCoder encodeObject:_slug forKey:kOAAuthorSlug];
    [aCoder encodeObject:_name forKey:kOAAuthorName];
    [aCoder encodeObject:_authorDescription forKey:kOAAuthorDescription];
}

- (id)copyWithZone:(NSZone *)zone
{
    OAAuthor *copy = [[OAAuthor alloc] init];
    
    if (copy) {

        copy.email = [self.email copyWithZone:zone];
        copy.authorIdentifier = self.authorIdentifier;
        copy.slug = [self.slug copyWithZone:zone];
        copy.name = [self.name copyWithZone:zone];
        copy.authorDescription = [self.authorDescription copyWithZone:zone];
    }
    
    return copy;
}


@end
