//
//  OARelatedNews.m
//
//  Created by Sourabh Goyal on 3/24/17
//  Copyright (c) 2017 __MyCompanyName__. All rights reserved.
//

#import "OARelatedNews.h"
#import "OAAuthor.h"


NSString *const kOARelatedNewsId = @"id";
NSString *const kOARelatedNewsCoverImage = @"coverImage";
NSString *const kOARelatedNewsAuthor = @"author";
NSString *const kOARelatedNewsBrand = @"brand";
NSString *const kOARelatedNewsPublishedAt = @"publishedAt";
NSString *const kOARelatedNewsUrl = @"url";
NSString *const kOARelatedNewsTitle = @"title";
NSString *const kOARelatedNewsViewCount = @"viewCount";
NSString *const kOARelatedNewsSlug = @"slug";
NSString *const kOARelatedNewsThumbnail = @"thumbnail";
NSString *const kOARelatedNewsModel = @"model";
NSString *const kOARelatedNewsContent = @"content";


@interface OARelatedNews ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation OARelatedNews

@synthesize relatedNewsIdentifier = _relatedNewsIdentifier;
@synthesize coverImage = _coverImage;
@synthesize author = _author;
@synthesize brand = _brand;
@synthesize publishedAt = _publishedAt;
@synthesize url = _url;
@synthesize title = _title;
@synthesize viewCount = _viewCount;
@synthesize slug = _slug;
@synthesize thumbnail = _thumbnail;
@synthesize model = _model;
@synthesize content = _content;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict
{
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if(self && [dict isKindOfClass:[NSDictionary class]]) {
            self.relatedNewsIdentifier = [[self objectOrNilForKey:kOARelatedNewsId fromDictionary:dict] doubleValue];
            self.coverImage = [self objectOrNilForKey:kOARelatedNewsCoverImage fromDictionary:dict];
            self.author = [OAAuthor modelObjectWithDictionary:[dict objectForKey:kOARelatedNewsAuthor]];
            self.brand = [self objectOrNilForKey:kOARelatedNewsBrand fromDictionary:dict];
            self.publishedAt = [self objectOrNilForKey:kOARelatedNewsPublishedAt fromDictionary:dict];
            self.url = [self objectOrNilForKey:kOARelatedNewsUrl fromDictionary:dict];
            self.title = [self objectOrNilForKey:kOARelatedNewsTitle fromDictionary:dict];
            self.viewCount = [[self objectOrNilForKey:kOARelatedNewsViewCount fromDictionary:dict] doubleValue];
            self.slug = [self objectOrNilForKey:kOARelatedNewsSlug fromDictionary:dict];
            self.thumbnail = [self objectOrNilForKey:kOARelatedNewsThumbnail fromDictionary:dict];
            self.model = [self objectOrNilForKey:kOARelatedNewsModel fromDictionary:dict];
            self.content = [self objectOrNilForKey:kOARelatedNewsContent fromDictionary:dict];

    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation
{
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:[NSNumber numberWithDouble:self.relatedNewsIdentifier] forKey:kOARelatedNewsId];
    if([self.coverImage isKindOfClass:[NSString class]]){
        [mutableDict setValue:[NSArray arrayWithObject:self.coverImage] forKey:kOARelatedNewsCoverImage];
    }
    else{
        NSMutableArray *tempArrayForCoverImage = [NSMutableArray array];
        for (NSObject *subArrayObject in self.coverImage) {
            if([subArrayObject respondsToSelector:@selector(dictionaryRepresentation)]) {
                // This class is a model object
                [tempArrayForCoverImage addObject:[subArrayObject performSelector:@selector(dictionaryRepresentation)]];
            } else {
                // Generic object
                [tempArrayForCoverImage addObject:subArrayObject];
            }
        }
        [mutableDict setValue:[NSArray arrayWithArray:tempArrayForCoverImage] forKey:kOARelatedNewsCoverImage];
    }
    [mutableDict setValue:[self.author dictionaryRepresentation] forKey:kOARelatedNewsAuthor];
    [mutableDict setValue:self.brand forKey:kOARelatedNewsBrand];
    [mutableDict setValue:self.publishedAt forKey:kOARelatedNewsPublishedAt];
    [mutableDict setValue:self.url forKey:kOARelatedNewsUrl];
    [mutableDict setValue:self.title forKey:kOARelatedNewsTitle];
    [mutableDict setValue:[NSNumber numberWithDouble:self.viewCount] forKey:kOARelatedNewsViewCount];
    [mutableDict setValue:self.slug forKey:kOARelatedNewsSlug];
    [mutableDict setValue:self.thumbnail forKey:kOARelatedNewsThumbnail];
    [mutableDict setValue:self.model forKey:kOARelatedNewsModel];
    [mutableDict setValue:self.content forKey:kOARelatedNewsContent];

    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description 
{
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict
{
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];

    self.relatedNewsIdentifier = [aDecoder decodeDoubleForKey:kOARelatedNewsId];
    self.coverImage = [aDecoder decodeObjectForKey:kOARelatedNewsCoverImage];
    self.author = [aDecoder decodeObjectForKey:kOARelatedNewsAuthor];
    self.brand = [aDecoder decodeObjectForKey:kOARelatedNewsBrand];
    self.publishedAt = [aDecoder decodeObjectForKey:kOARelatedNewsPublishedAt];
    self.url = [aDecoder decodeObjectForKey:kOARelatedNewsUrl];
    self.title = [aDecoder decodeObjectForKey:kOARelatedNewsTitle];
    self.viewCount = [aDecoder decodeDoubleForKey:kOARelatedNewsViewCount];
    self.slug = [aDecoder decodeObjectForKey:kOARelatedNewsSlug];
    self.thumbnail = [aDecoder decodeObjectForKey:kOARelatedNewsThumbnail];
    self.model = [aDecoder decodeObjectForKey:kOARelatedNewsModel];
    self.content = [aDecoder decodeObjectForKey:kOARelatedNewsContent];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{

    [aCoder encodeDouble:_relatedNewsIdentifier forKey:kOARelatedNewsId];
    [aCoder encodeObject:_coverImage forKey:kOARelatedNewsCoverImage];
    [aCoder encodeObject:_author forKey:kOARelatedNewsAuthor];
    [aCoder encodeObject:_brand forKey:kOARelatedNewsBrand];
    [aCoder encodeObject:_publishedAt forKey:kOARelatedNewsPublishedAt];
    [aCoder encodeObject:_url forKey:kOARelatedNewsUrl];
    [aCoder encodeObject:_title forKey:kOARelatedNewsTitle];
    [aCoder encodeDouble:_viewCount forKey:kOARelatedNewsViewCount];
    [aCoder encodeObject:_slug forKey:kOARelatedNewsSlug];
    [aCoder encodeObject:_thumbnail forKey:kOARelatedNewsThumbnail];
    [aCoder encodeObject:_model forKey:kOARelatedNewsModel];
    [aCoder encodeObject:_content forKey:kOARelatedNewsContent];
}

- (id)copyWithZone:(NSZone *)zone
{
    OARelatedNews *copy = [[OARelatedNews alloc] init];
    
    if (copy) {

        copy.relatedNewsIdentifier = self.relatedNewsIdentifier;
        copy.coverImage = [self.coverImage copyWithZone:zone];
        copy.author = [self.author copyWithZone:zone];
        copy.brand = [self.brand copyWithZone:zone];
        copy.publishedAt = [self.publishedAt copyWithZone:zone];
        copy.url = [self.url copyWithZone:zone];
        copy.title = [self.title copyWithZone:zone];
        copy.viewCount = self.viewCount;
        copy.slug = [self.slug copyWithZone:zone];
        copy.thumbnail = [self.thumbnail copyWithZone:zone];
        copy.model = [self.model copyWithZone:zone];
        copy.content = [self.content copyWithZone:zone];
    }
    
    return copy;
}


@end
