//
//  OANewsDetailModel.h
//
//  Created by Sourabh Goyal on 3/24/17
//  Copyright (c) 2017 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@class OAData;

@interface OANewsDetailModel : NSObject <NSCoding, NSCopying>

@property (nonatomic, assign) BOOL status;
@property (nonatomic, assign) double statusCode;
@property (nonatomic, strong) NSString *statusText;
@property (nonatomic, strong) OAData *data;

+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict;
- (instancetype)initWithDictionary:(NSDictionary *)dict;
- (NSDictionary *)dictionaryRepresentation;

@end
