//
//  NetworkManager.h
//  NetworkLayer
//
//  Created by VS on 02/09/16.
//  Copyright © 2016 VS. All rights reserved.
//


#import <Foundation/Foundation.h>
#import <NetworkLayer/NetworkConstants.h>

@class NetworkManager;
@protocol NetworkManagerDelegate;

//@protocol NetworkManagerDelegate <NSObject>
//
//- (void)internetAvailabilityWithStatus:(InternetReachabilityStatus)status;
//
//@end


typedef NS_ENUM(NSInteger, NetworkManagerSerializer) {
    NetworkManagerSerializerJSON,
    NetworkManagerSerializerHTTP
};


@interface NetworkManager : NSObject<NSURLSessionDelegate>


+ (void)setServerRequestCommonParameters:(NSDictionary *)commonParametersDictionary;

+ (void)setRequestHeaderWithDictionary:(NSDictionary *)requestHeader;


/*
 * Error code 2059 means no internet available
 */
- (void)getServerDataWithUrl:(NSString *)urlString
           requestParameters:(NSDictionary *)params
              bodyParameters:(id)bodyParams
               apiMethodType:(NSString *)methodType
              andMapperClass:(Class)mapperClass
                  completion:(ResponseBlock)completionBlock;

- (void)getServerDataWithUrl:(NSString *)urlString
           requestParameters:(NSDictionary *)params
              bodyParameters:(id)bodyParams
               apiMethodType:(NSString *)methodType
              andMapperClass:(Class)mapperClass
               andSerializer:(NetworkManagerSerializer)serializer
                  completion:(ResponseBlock)completionBlock;



//@property (weak, nonatomic) id<NetworkManagerDelegate> delegate;

@end





