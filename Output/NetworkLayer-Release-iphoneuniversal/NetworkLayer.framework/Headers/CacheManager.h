//
//  CacheManager.h
//  NetworkLayer
//
//  Created by VS on 09/11/16.
//  Copyright © 2016 VS. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <NetworkLayer/NetworkConstants.h>


@interface CacheManager : NSObject


//Input Size in bytes
+ (void)setAllowedCacheSize:(unsigned long long)cacheSize;

+ (void)setMaximumFilesInCache:(NSUInteger)maximumCacheFiles;

+ (unsigned long long)cacheSize;

+ (NSUInteger)maximumNumberOfFilesInCache;

+ (NSString *) applicationDocumentsDirectory;

+ (NSString *)apiCacheDirectoryPath;
+ (void)cleanCacheFiles;
- (void)getCacheDataForMapperClass:(Class)mapperClass
                           withUrl:(NSString *)url
                     requestHeader:(NSDictionary *)header
                     requestParams:(NSDictionary *)params
                        bodyParams:(id)bodyParams
                     apiMethodType:(NSString *)methodType
                        completion:(CacheResponseBlock)completionBlock;

//Not meant to be called from outside this framework
- (void)writeMapperToCacheWithMapperClass:(Class)mapperClass
                             responseData:(NSData *)responseData
                                      url:(NSString *)url
                            apiMethodType:(NSString *)methodType
                            requestHeader:(NSDictionary *)header
                                paramDict:(NSDictionary *)params
                            andBodyParams:(id)bodyParams;





@end

