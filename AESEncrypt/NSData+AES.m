//
//  NSData+AES.m
//  Safebox
//
//  Created by Juan Ignacio Laube on 16/03/13.
//  Copyright (c) 2013 Juan Ignacio Laube. All rights reserved.
//

#import <CommonCrypto/CommonCryptor.h>
#import <CommonCrypto/CommonKeyDerivation.h>
#import "Base64.h"
#import "NSData+AES.h"

const CCAlgorithm kAlgorithm = kCCAlgorithmAES;
const NSUInteger kAlgorithmKeySize = kCCKeySizeAES128;
const NSUInteger kAlgorithmBlockSize = kCCBlockSizeAES128;
const NSUInteger kAlgorithmIVSize = kCCBlockSizeAES128;
const NSUInteger kPBKDFSaltSize = 8;
const NSUInteger kPBKDFRounds = 10000;

const NSString *password_new = @"C@rdekh0D0tCoMap";
const NSString *initialVectorString = @"hdfsea_appvahang";
const NSString *saltString = @"ThisIsASecretKey";

@implementation NSData (AES)



#pragma mark Methods needed in CBC crypto.

+ (NSData *)createRandomInitializationVector
{
    NSData *data = [NSData createRandomDataWithLengh:kCCBlockSizeAES128];
    return data;
}

+ (NSData *)createRandomSalt
{
    NSData *data = [NSData createRandomDataWithLengh:8];
    return data;
}

+ (NSString *)AESEncryptedDataForData:(NSData *)data withPassword:(NSString *)password iv:(NSData *)iv salt:(NSData *)salt error:(NSError **)error
{
    return [data AESEncryptWithPassword:password iv:iv salt:salt error:error];
}

+ (NSData *)dataForAESEncryptedData:(NSData *)data withPassword:(NSString *)password iv:(NSData *)iv salt:(NSData *)salt error:(NSError **)error
{
    return [data AESDecryptWithPassword:password iv:iv salt:salt error:error];
}


- (NSString *)AESEncryptWithPassword:(NSString *)password iv:(NSData *)iv salt:(NSData *)salt error:(NSError **)error
{
    
    NSData *iv_new = [initialVectorString dataUsingEncoding:NSUTF8StringEncoding];
    NSData *salt_new = [saltString dataUsingEncoding:NSUTF8StringEncoding];
    
    NSData *key = [NSData keyForPassword:(NSString *)password_new salt:salt_new];
    
    size_t outLength;
    NSMutableData *cipherData = [NSMutableData dataWithLength:self.length + kAlgorithmBlockSize];
    
    CCCryptorStatus result = CCCrypt(kCCEncrypt, kAlgorithm, kCCOptionPKCS7Padding, key.bytes, key.length, iv_new.bytes, self.bytes, self.length, cipherData.mutableBytes, cipherData.length, &outLength);
    
    if(result == kCCSuccess)
    {
        cipherData.length = outLength;
    }
    else if(error)
    {
        *error = [NSError errorWithDomain:@"com.ios.vahangyan" code:result userInfo:nil];
        return nil;
    }
    return [Base64 encode:cipherData];
}

- (NSData *)AESDecryptWithPassword:(NSString *)password iv:(NSData *)iv salt:(NSData *)salt error:(NSError **)error
{
    
    
    NSData *iv_new = [initialVectorString dataUsingEncoding:NSUTF8StringEncoding];
    NSData *salt_new = [saltString dataUsingEncoding:NSUTF8StringEncoding];

    
    NSData *key = [NSData keyForPassword:(NSString *)password_new salt:salt_new];
    size_t outLength;
    NSMutableData *clearData = [NSMutableData dataWithLength:self.length + kAlgorithmBlockSize];
    
    CCCryptorStatus result = CCCrypt(kCCDecrypt, kAlgorithm, kCCOptionPKCS7Padding, key.bytes, key.length, iv_new.bytes, self.bytes, self.length, clearData.mutableBytes, clearData.length, &outLength);
    
    if(result == kCCSuccess)
    {
        clearData.length = outLength;
    }
    else if(error)
    {
        *error = [NSError errorWithDomain:@"com.ios.vahangyan" code:result userInfo:nil];
        return nil;
    }
    
    // NSString *string = [[NSString alloc] initWithData:clearData encoding:NSUTF8StringEncoding];
    
   return clearData;
}

+ (NSData *)createRandomDataWithLengh:(NSUInteger)length
{
    NSMutableData *data = [[NSMutableData alloc] initWithLength:length];
    SecRandomCopyBytes(kSecRandomDefault, length, data.mutableBytes);
    return data;
}

+ (NSData *)keyForPassword:(NSString *)password salt:(NSData *)salt
{
    NSMutableData *key = [NSMutableData dataWithLength:kAlgorithmKeySize];
    int
    result =CCKeyDerivationPBKDF(kCCPBKDF2, password.UTF8String, [password lengthOfBytesUsingEncoding:NSUTF8StringEncoding], salt.bytes, salt.length, kCCPRFHmacAlgSHA1, kPBKDFRounds, key.mutableBytes, key.length);
    
    // Do not log password here
    NSAssert(result == kCCSuccess,
             @"Unable to create AES key for password: %d", result);

    
    
    
    
    return key;
}


@end
