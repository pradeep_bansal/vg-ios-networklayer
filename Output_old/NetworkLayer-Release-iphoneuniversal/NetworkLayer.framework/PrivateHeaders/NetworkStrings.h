//
//  NetworkStrings.h
//  NetworkLayer
//
//  Created by VS on 02/09/16.
//  Copyright © 2016 VS. All rights reserved.
//

#import <Foundation/Foundation.h>

#define kCacheDoesntExistMsg @"Cache for mapperClass: \"%@\" at File Path \"%@\" doesn't exist", mapperClass, filePath
#define kCachingNotAllowedMsg @"Caching not allowed for mapperClass: \%@\"", mapperClass
#define kCacheFileNonExistentMsg @"Cache File doesn't exist at path", filePath
#define kCacheFileNonExistentError @"Non Existent Cache File Error"
#define kClassNotSubclassingAPIMapperMsg @"class: %@ is not a subclass of APIMapper", mapperClass
#define kMapperClassValidatedMsg @"mapper class: %@ validated", mapperClass
#define kUrlStringInvalidMsg @"Error:::::: Invalid URL"
#define kUrlStringValidatedMsg @"URL validated: %@", urlString
#define kAboutToSendErrorToCompletionBlockMsg @"Error: %@ about to be sent to the Completion block", error
#define kMapperValidatedMsg @"Mapper class: %@ validated", mapperClass
#define kUrlValidatedMsg @"URL without paramaters: %@ validated", [mapperClass urlString]
#define kAboutToSendMapperToCompletionBlockMsg @"Mapper instance of class %@ about to be sent to the Completion block", [mapper class]
#define kSettingCommonParametersMsg @"Setting common parameters in dictionary: %@", _parameters
#define kCommonParametersSetMsg @"Setting following common paramaters dictionary: %@", dict
#define kCommonParametersNotSetMsg @"No default parameters provided"
#define kSettingCacheFilePathMsg @"Setting cache filePath: %@", filePath
#define kCachingAllowedMsg @"Caching allowed."
#define kCheckingCacheMsg @"Checking cache."
#define kWritingToCacheMsg @"Writing to cache the mapper for mapperClass: %@", mapperClass

#define kCacheFileExistsMsg @"Cache file at path: %@ exists. About to read it's contents.", filePath
#define kErrorReadingContentsOfFileMsg @"Error in reading file at file path: %@. Error: %@", filePath, *err
#define kCacheMappingError @"Error in Mapping from Cache, error = %@", error
#define kErrorMappingCacheToMapperMsg @"Error in mapping cache at file path: %@ to corresponding mapper. Error: %@", filePath, *err
#define kMapperMappedFromCacheMsg @"Mapping from existing cache file at file path: %@ for mapperClass: %@", filePath, NSStringFromClass(mapperClass)
#define kMapperMappedFromAPIResponseMsg @"Mapped Sucessfully to the mapper class; %@", NSStringFromClass(mapperClass)
#define kMapperCouldNotBeMappedFromAPIResponseMsg @"Could Not Map Response to the mapper class; %@", NSStringFromClass(mapperClass)
#define kUncaughtErrorInReadingCacheMsg @"Uncaught exception while reading file from mapper cache at filepath: %@ for mapperclass: %@", filePath, mapperClass
#define kGotAnAPIResponseMsg @"Got an API Response, json: %@. About to map the mapper.", json
#define kNoErrorMsg @"No error in response"
#define kErrorInWritingCache @"Error in writing cache file at path: %@. Error: %@", filePath, errorInWritingCache
#define kCacheWrittenMsg @"Successfully written cache at filePath: %@", filePath
#define kLoadingFromRemoteMsg @"Loading from remote"
#define kErrorMappingAPIResponseToMapperMsg @"Error mapping API response to mapper for mapper class %@: %@", mapperClass, error
#define kNotDictionaryTypeOfResponseMsg @"Received the folowing json response that is not a NSDictionary type of class: %@", json
#define kErrorResponseFromAPI @"Error response from API. Error: %@", err
#define kUnhandledErrorForCacheMsg @"Unhandled error while reading cache"
#define kCachingNotAllowedError @"CachingNotAllowedError"
#define kServiceCallSuccessForURLMsg @"Service Call Success for URL = %@",task.currentRequest.URL.absoluteString
#define kRequestParametersMsg @"Request Parameters:%@", _parameters
#define kInternetNotAvailable @"Internet Not Available"
#define kGETMethod @"GET"
#define kHEADMethod @"HEAD"
#define kPOSTMethod @"POST"
#define kPUTMethod @"PUT"
#define kDELETEMethod @"DELETE"


#define kServerSuccessResponseMsg @"Service Call Success, URL = %@",task.currentRequest.URL.absoluteString
#define kGETServiceCallFailure @"GET Service Call Faliure, URL = %@",operation.currentRequest.URL.absoluteString
#define kPOSTServiceCallFailure @"POST Service Call Faliure, URL = %@", task.currentRequest.URL.absoluteString

#define kCacheFileDeletedMsg @"Successfully deleted cache file. %@", filePath
#define kErrorDeletingCacheFileMsg @"Error deleting cache file: %@. Error: %@", filePath, error
#define kDirectorySizeCalculationError @"Error in calculating directory size: %@", error
#define kNoFilesInCacheDirectory @"No files in cache cache directory."

#define kCacheFileDeletionStatusMsg @"Cache file %@deleted", fileDeleted?@"":@"NOT "
#define kCacheInfoJSONFileUpdatedMsg @"Cache Info JSON File %@Updated.", cacheInfoJSONFileUpdated?@"":@"NOT "
#define kErrorInWritingCacheInfoFileMsg @"Error in writing cache Info file at path: %@. Error: %@", filePath, errorInWritingCacheInfoFile
#define kCacheInfoFileWrittenMsg @"Successfully written cache info file in main bundle, at file path: %@", filePath
#define kNetworkNotReachableError @"Network Not Reachable"

#define kModelParsingErrorMsg @"Model Parsing error"
#define kNullResponseFromServer @"Empty Response From Server"
#define kHTTPStatusCodeMsg @"HTTP Status Code: %li", (long)_httpStatusCode

@interface NetworkStrings : NSObject

@end
