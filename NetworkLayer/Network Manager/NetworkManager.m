//
//  NetworkManager.m
//  NetworkLayer
//
//  Created by VS on 02/09/16.
//  Copyright © 2016 VS. All rights reserved.
//

#import <NetworkLayer/NetworkLayer.h>
#import <NetworkLayer/Reachability.h>
#import <NetworkLayer/CacheManager.h>
#import <NetworkLayer/NetworkErrorLogger.h>
#import "NSData+MD5Crypto.h"
#import "NSObject+Cache.h"
#import "NSData+AES.h"
#import "CWStatusBarNotification.h"



#define kNoUrlError @"No url specified!"
#define kEmptyString @""
#define kURLUnavailable @"Url \"%@\" unavailable", urlString
#define kAPIBodyKey @"bodystring"


static NSMutableDictionary *_commonParamDict =nil;
static NSMutableDictionary *_requestHeader = nil;


@interface NetworkManager()

@property (nonatomic, strong)  CWStatusBarNotification *statusBarNotification;

+ (NSMutableDictionary *)commonParameters;

+ (NSMutableDictionary *)requestHeader;

- (NetworkStatus)getReachabiltyStatus;

- (NSError *)errorWithDomain:(NSString *)urlString
                        code:(NSInteger)code
                 description:(NSString *)description;

@end


@implementation NetworkManager {
    
    NSMutableDictionary *_parameters;
    CacheManager *_cacheManager;
    long int _httpStatusCode;
}


+ (void)initialize
{
    static dispatch_once_t once;
    dispatch_once(&once, ^{
        _commonParamDict = [[NSMutableDictionary alloc] init];
        _requestHeader = [[NSMutableDictionary alloc] init];
    });
}

- (instancetype)init {
    if (self = [super init]) {
    }
    return self;
}

-(CWStatusBarNotification *)statusBarNotification{
    if (_statusBarNotification == nil){
        _statusBarNotification = [CWStatusBarNotification new];
        _statusBarNotification.notificationAnimationInStyle = CWNotificationAnimationStyleTop;
        _statusBarNotification.notificationAnimationOutStyle = CWNotificationAnimationStyleTop;
        _statusBarNotification.notificationLabelBackgroundColor = [UIColor colorWithRed:255/255.0f green:75/255.0f blue:39/255.0f alpha:1];
        _statusBarNotification.notificationLabelTextColor = [UIColor whiteColor];
    }
    return _statusBarNotification;
}


#pragma mark - Public method that sets Common parameters for requests

+ (void)setServerRequestCommonParameters:(NSDictionary *)commonParametersDictionary {
    
    NSMutableDictionary *dict = [commonParametersDictionary  mutableCopy];
    
    NSArray *keys = [dict allKeys];
    for (NSString *key in keys) {
        [[NetworkManager commonParameters] setValue:[dict valueForKey:key]
                                             forKey:key];
    }
}


+ (void)setRequestHeaderWithDictionary:(NSDictionary *)headerDictionary {
    
    for (id headerKey in [NSArray arrayWithArray:[headerDictionary allKeys]]) {
        
        NSString *headerValue = [headerDictionary valueForKey:headerKey];
        [_requestHeader setValue:headerValue forKey:headerKey];
    }
}


#pragma mark - Instance methods to get the data from Server or cache

- (void)getServerDataWithUrl:(NSString *)urlString
           requestParameters:(NSDictionary *)params
              bodyParameters:(id)bodyParams
               apiMethodType:(NSString *)methodType
              andMapperClass:(Class)mapperClass
                  completion:(ResponseBlock)completionBlock {
    [self getServerDataWithUrl:urlString requestParameters:params bodyParameters:bodyParams apiMethodType:methodType andMapperClass:mapperClass andSerializer:NetworkManagerSerializerJSON completion:completionBlock];
}

- (void)getServerDataWithUrl:(NSString *)urlString
           requestParameters:(NSDictionary *)params
              bodyParameters:(id)bodyParams
               apiMethodType:(NSString *)methodType
              andMapperClass:(Class)mapperClass
               andSerializer:(NetworkManagerSerializer)serializer
                  completion:(ResponseBlock)completionBlock{
    
    //Cache
    _cacheManager = [[CacheManager alloc] init];
    [_cacheManager getCacheDataForMapperClass:mapperClass
                                      withUrl:urlString
                                requestHeader:_requestHeader
                                requestParams:params
                                   bodyParams:bodyParams
                                apiMethodType:methodType
                                   completion:^(NSObject *mapper, NSString *hashString, NSError *error) {
                                       if (mapper) {
                                           NSLog(@"::::::Response From Cashe::::::::");
                                           [self sendResponse:mapper withError:nil withAuthorization:nil toCallBack:completionBlock];
                                       }
                                       
                                       //Rechablity
                                       if([self getReachabiltyStatus] == NotReachable)
                                       {
                                           if(mapper)
                                               return;
                                           NSLog(@"::::::No internet connection !::::::::");
                                           [self.statusBarNotification displayNotificationWithMessage:@"No internet connection !" forDuration:1.0];
                                           NSError *error = [self errorWithDomain:urlString
                                                                             code:0
                                                                      description:kInternetNotAvailable];
                                           [self sendResponse:nil withError:error withAuthorization:nil toCallBack:completionBlock];
                                       }
                                       else{
                                           NSLog(@"::::::Calling Web Service::::::::");
                                           [self getWebServerDataWithUrl:urlString requestParameters:params bodyParameters:bodyParams apiMethodType:methodType andMapperClass:mapperClass andSerializer:serializer hashString:hashString completion:completionBlock];
                                       }
                                   }];
}


- (void)getWebServerDataWithUrl:(NSString *)urlString
              requestParameters:(NSDictionary *)params
                 bodyParameters:(id)bodyParams
                  apiMethodType:(NSString *)methodType
                 andMapperClass:(Class)mapperClass
                  andSerializer:(NetworkManagerSerializer)serializer
                     hashString:(NSString *)hashString
                     completion:(ResponseBlock)completionBlock{
    
    NSString *newUrlString =[urlString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSURL *requestURL = [NSURL URLWithString:newUrlString];
    if(requestURL == nil){
        //Invalid URL
        NSError *error = [self errorWithDomain:urlString
                                          code:0
                                   description:kUrlStringInvalidMsg];
        [self sendResponse:nil withError:error withAuthorization:nil toCallBack:completionBlock];
    }
    else{
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:requestURL];
        request.HTTPMethod = methodType;
        if(serializer == NetworkManagerSerializerHTTP){
            [request addValue:@"8bit" forHTTPHeaderField:@"Content-Transfer-Encoding"];
            NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@",[self boundary]];
            [request addValue:contentType forHTTPHeaderField:@"Content-Type"];
        }
        else{
            [request setValue:@"application/json; charset=utf-8" forHTTPHeaderField:@"Content-Type"];
        }
        NSDictionary *dictCommonHeaders = [NetworkManager requestHeader];
        for (NSString *key in dictCommonHeaders.allKeys) {
            NSString *value = [dictCommonHeaders objectForKey:key];
            [request setValue:value forHTTPHeaderField:key];
        }
        
        NSMutableDictionary *dictRequestParameters = [NSMutableDictionary new];
        NSDictionary *dictCommonParameters = [NetworkManager commonParameters];
        if(dictCommonParameters){
            [dictRequestParameters addEntriesFromDictionary:dictCommonParameters];
        }
        if(params){
            [dictRequestParameters addEntriesFromDictionary:params];
        }
        
        if([bodyParams isKindOfClass:[NSDictionary class]] && [methodType isEqualToString:kGETMethod]){
            [dictRequestParameters addEntriesFromDictionary:bodyParams];
            bodyParams = dictRequestParameters;
        }
        NSMutableString *strQueryParameters = [NSMutableString string];
        for (NSString *key in dictRequestParameters.allKeys) {
            NSString *value = [dictRequestParameters objectForKey:key];
            if([strQueryParameters isEqualToString:@""]){
                [strQueryParameters appendString:@"?"];
            }
            else{
                [strQueryParameters appendString:@"&"];
            }
            [strQueryParameters appendFormat:@"%@=%@",key,value];
        }
        
        NSString *completeURL = [NSString stringWithFormat:@"%@%@", requestURL.absoluteString, strQueryParameters];
        completeURL =[completeURL stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        requestURL = [NSURL URLWithString:completeURL];
        request.URL = requestURL;
        
        NSLog(@"Request URL : %@",requestURL.absoluteString);
        BOOL isEncrypt = NO;
            if([methodType isEqualToString:kPOSTMethod] && bodyParams != nil){
                NSMutableData *requestBodyData;
                if(serializer == NetworkManagerSerializerHTTP){
                    requestBodyData = [NSMutableData data];
                    NSArray *allKeys = [(NSDictionary *)bodyParams allKeys];
                    for (NSString *key in allKeys) {
                        [requestBodyData appendData:[[NSString stringWithFormat:@"--%@\r\nContent-Disposition: form-data; name=\"%@\"\r\n\r\n%@\r\n",[self boundary],key,[(NSDictionary *)bodyParams objectForKey:key]] dataUsingEncoding:NSUTF8StringEncoding]];
                    }
                    [requestBodyData appendData:[[NSString stringWithFormat:@"--%@--\r\n", [self boundary]] dataUsingEncoding:NSUTF8StringEncoding]];
                }
                else{
                    if(nil != bodyParams && [bodyParams isKindOfClass:[NSDictionary class]] &&  nil != [bodyParams valueForKey:@"info"]){
                        NSDictionary *infoDict = [bodyParams valueForKey:@"info"];
                        if(nil != infoDict){
                            if(nil != [infoDict valueForKey:@"encrypt"] && [[infoDict valueForKey:@"encrypt"] isEqualToString:@"y"]){
                                isEncrypt = YES;
                            }else{
                                isEncrypt = NO;
                            }
                        }else{
                            isEncrypt = NO;
                        }
                    }
                    requestBodyData = (NSMutableData *)[NSJSONSerialization dataWithJSONObject:bodyParams options:NSJSONWritingPrettyPrinted error:nil];
                }
                
                NSString *inputData = [[NSString alloc] initWithData:requestBodyData encoding:NSUTF8StringEncoding];
                NSLog(@"request Data : %@",inputData);
                if(isEncrypt){
                    NSData *data = [inputData  dataUsingEncoding:NSUTF8StringEncoding];
                    NSString * inputaDataEncrypt = [NSData AESEncryptedDataForData:data withPassword:nil iv:nil salt:nil error:nil];
                    requestBodyData = [[NSMutableData alloc]  initWithData:[inputaDataEncrypt  dataUsingEncoding:NSUTF8StringEncoding]];
                }
                request.HTTPBody = requestBodyData;
                NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[requestBodyData length]];
                [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
            }
        
        request.timeoutInterval = 30.0;
        
        NSURLSession *defaultSession = [NSURLSession sessionWithConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration] delegate:self delegateQueue:nil];
        NSURLSessionDataTask *dataTask = [defaultSession dataTaskWithRequest:request completionHandler:^(NSData * data, NSURLResponse * response, NSError * connectionError) {
            NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *)response;
            NSLog(@"HTTP Status Code: %ld", (long)httpResponse.statusCode);
            if (httpResponse.statusCode != 200){
                
                [self sendResponse:nil withError:connectionError withAuthorization:nil toCallBack:completionBlock];
                
                // [self getWebServerDataWithUrl:@"http://newcarsapi.gaadi.com/v1/log/errors?format=json&country_code=in&lang_code=en&business_unit=car" requestParameters:nil bodyParameters:(id) apiMethodType:@"POST" andMapperClass:<#(__unsafe_unretained Class)#> andSerializer:serializer hashString:<#(NSString *)#> completion:nil];
            }
            else if (data == nil){
                NSError *error = [self errorWithDomain:urlString
                                                  code:0
                                           description:kNullResponseFromServer];
                [self sendResponse:nil withError:error withAuthorization:nil toCallBack:completionBlock];
            }
            else{
                // Get Response Header Authorization Key
                NSDictionary *responseHeaderDict = httpResponse.allHeaderFields;
                NSString *authorizationKey = [responseHeaderDict objectForKey:@"Authorization"];
                
                if([mapperClass printLogs]){
                    NSString *strData = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
                    NSLog(@"Got Response : %@",strData);
                }
                NSString *newHashString = [data MD5String];
                if([hashString isEqualToString:newHashString]){
                    NSLog(@":::::::Same response from server Response:::::::");
                    return;
                }
                NSString *strData = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
                if ([strData rangeOfString:@"status"].location == NSNotFound){
                    strData = [NetworkManager getOriginalStringFromSpecialCharacter:strData];
                    NSData *decodedData = [[NSData alloc] initWithBase64EncodedString:strData options:0];
                    data = [NSData dataForAESEncryptedData:decodedData withPassword:nil iv:nil salt:nil error:nil];
                }
                NSError *error;
                NSDictionary *jsonResult = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&error];
                
                if(([strData rangeOfString:@"109"].location == NSNotFound) &&([jsonResult[@"status"] isKindOfClass:[NSNull class]] && [jsonResult[@"data"] isKindOfClass:[NSNull class]])){
                    NSError *error = [self errorWithDomain:urlString
                                                      code:0
                                               description:kModelParsingErrorMsg];
                    [self sendResponse:nil withError:error withAuthorization:authorizationKey toCallBack:completionBlock];
                    return;

                }
                
                
                NSLog(@":::::::response Data:::::::%@",jsonResult);
                
                id mapper  = [mapperClass alloc];
                if(mapperClass != [NSDictionary class] && [mapper respondsToSelector:@selector(initWithDictionary:)]){
                    mapper = [mapper initWithDictionary:jsonResult];
                }
                else{
                    mapper = [[mapperClass alloc] initWithJsonObject:jsonResult];
                }
                
                //statusCode
                //success
                if([jsonResult[@"status"] isKindOfClass:[NSDictionary class]] && jsonResult[@"status"]){
                    NSDictionary * statusDict = jsonResult[@"status"];
                    if([statusDict[@"code"] integerValue] == 109){ // Session TimeOUt
                        dispatch_async(dispatch_get_main_queue(), ^{
                            [[NSNotificationCenter defaultCenter] postNotificationName:@"SessionTimeOut" object:self userInfo:statusDict];
                        });
                        [self sendResponse:nil withError:error withAuthorization:authorizationKey toCallBack:completionBlock];
                        return;
                    }
                }else if (jsonResult[@"success"]){
                    if([jsonResult[@"success"] boolValue] == NO){
                        NSError *error = [self errorWithDomain:@"Invalid Input Parameters !"
                                                          code:0
                                                   description:kModelParsingErrorMsg];
                        [self sendResponse:nil withError:error withAuthorization:authorizationKey toCallBack:completionBlock];
                        return;
                    }
                }
                else if (jsonResult[@"statusCode"]){
                    if([jsonResult[@"statusCode"] integerValue] != 200){
                        NSError *error = [self errorWithDomain:jsonResult[@"statusText"]
                                                          code:0
                                                   description:kModelParsingErrorMsg];
                        [self sendResponse:nil withError:error withAuthorization:authorizationKey toCallBack:completionBlock];
                        return;
                    }
                }
                if(mapper){
                    OALog(kMapperMappedFromAPIResponseMsg);
                    
                    [_cacheManager writeMapperToCacheWithMapperClass:mapperClass
                                                        responseData:data
                                                                 url:urlString
                                                       apiMethodType:methodType
                                                       requestHeader:_requestHeader
                                                           paramDict:params
                                                       andBodyParams:bodyParams];
                    
                    [self sendResponse:mapper withError:nil withAuthorization:authorizationKey toCallBack:completionBlock];
                }
                else{
                    NSError *error = [self errorWithDomain:urlString
                                                      code:0
                                               description:kModelParsingErrorMsg];
                    [self sendResponse:nil withError:error withAuthorization:authorizationKey toCallBack:completionBlock];
                }
            }
        }];
        [dataTask resume];
    }
    
}

-(NSString *)boundary{
    return @"************";
}
#pragma mark - NSURLSession delegate

- (void)URLSession:(NSURLSession *)session didReceiveChallenge:(NSURLAuthenticationChallenge *)challenge completionHandler:(void (^)(NSURLSessionAuthChallengeDisposition disposition, NSURLCredential *credential))completionHandler
{
    NSString *method = challenge.protectionSpace.authenticationMethod;
    NSLog(@"%@", method);
    
    if([method isEqualToString:NSURLAuthenticationMethodServerTrust]){
        
        NSString *host = challenge.protectionSpace.host;
        NSLog(@"%@", host);
        
        NSURLCredential *credential = [NSURLCredential credentialForTrust:challenge.protectionSpace.serverTrust];
        completionHandler(NSURLSessionAuthChallengeUseCredential, credential);
        return;
    }
    
    NSString *thePath = [[NSBundle mainBundle] pathForResource:@"client_certificate" ofType:@"p12"];//p12 working fine
    NSData *PKCS12Data = [[NSData alloc] initWithContentsOfFile:thePath];
    CFDataRef inPKCS12Data = (CFDataRef)CFBridgingRetain(PKCS12Data);
    SecIdentityRef identity;
    
    // Read the p12 certificate content
    OSStatus result = [self extractP12Data:inPKCS12Data toIdentity:&identity];
    if(result != errSecSuccess){
        completionHandler(NSURLSessionAuthChallengeCancelAuthenticationChallenge, nil);
        return;
    }
    
    SecCertificateRef certificate = NULL;
    SecIdentityCopyCertificate (identity, &certificate);
    
    const void *certs[] = {certificate};
    CFArrayRef certArray = CFArrayCreate(kCFAllocatorDefault, certs, 1, NULL);
    
    NSURLCredential *credential = [NSURLCredential credentialWithIdentity:identity certificates:(NSArray*)CFBridgingRelease(certArray) persistence:NSURLCredentialPersistencePermanent];
    
    completionHandler(NSURLSessionAuthChallengeUseCredential, credential);
}

-(OSStatus) extractP12Data:(CFDataRef)inP12Data toIdentity:(SecIdentityRef*)identity {
    
    OSStatus securityError = errSecSuccess;
    
    CFStringRef password = CFSTR("vahangyan");
    const void *keys[] = { kSecImportExportPassphrase };
    const void *values[] = { password };
    
    CFDictionaryRef options = CFDictionaryCreate(NULL, keys, values, 1, NULL, NULL);
    
    CFArrayRef items = CFArrayCreate(NULL, 0, 0, NULL);
    securityError = SecPKCS12Import(inP12Data, options, &items);
    
    if (securityError == 0) {
        CFDictionaryRef ident = CFArrayGetValueAtIndex(items,0);
        const void *tempIdentity = NULL;
        tempIdentity = CFDictionaryGetValue(ident, kSecImportItemIdentity);
        *identity = (SecIdentityRef)tempIdentity;
    }
    
    if (options) {
        CFRelease(options);
    }
    
    return securityError;
}

- (void)sendResponse:(NSObject *)response withError:(NSError *)error withAuthorization:(NSString *)authorization toCallBack:(ResponseBlock)completionBlock{
    dispatch_async(dispatch_get_main_queue(), ^{
        //        completionBlock(response, error);
        completionBlock(response, authorization, error);
    });
}

#pragma mark - Helper instance methods for getServerDataForMapper method

- (BOOL)isUrlStringValid:(NSString *)urlString {
    
    if(urlString){
        OALog(kUrlStringValidatedMsg);
        
        return YES;
    }
    OALog(kUrlStringInvalidMsg);
    return NO;
}


#pragma mark - Private Helper Instance methods to return specific NSError instance

- (NSError *)mapperNotFormedErrorForMapperClass:(Class)mapperClass
                                 withRequestUrl:(NSString *)urlString {
    
    NSString *errorDescription = [NSString
                                  stringWithFormat:kMapperCouldNotBeMappedFromAPIResponseMsg];
    
    return [self errorWithDomain:urlString
                            code:0
                     description:errorDescription];
}


- (NSError *)internetNotAvailableError {
    
    NSString *errorDescription = kInternetNotAvailable;
    
    return [self errorWithDomain:kInternetNotAvailableError
                            code:2059
                     description:errorDescription];
}


/**
 *
 * Private helper method that returns an NSError type instance
 * @parameters: urlString -- The url string for which this error is getting created
 * code -- error code
 * description -- description of the error
 *
 */
- (NSError *)errorWithDomain:(NSString *)urlString
                        code:(NSInteger)code
                 description:(NSString *)description {
    
    //TODO:: Discussion required over error codes
    
    NSMutableDictionary* details = [NSMutableDictionary dictionary];
    
    //If no url is specified
    if (urlString.length == 0 || urlString == nil) {
        urlString = kEmptyString;
        description = kNoUrlError;
    }
    
    [details setValue:description
               forKey:NSLocalizedDescriptionKey];
    
    return [NSError errorWithDomain:urlString
                               code:code
                           userInfo:details];
}


#pragma mark - Internet Reachability

- (NetworkStatus)getReachabiltyStatus {
    
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [reachability currentReachabilityStatus];
    
    return networkStatus;
    
}


#pragma mark - Private class methods returning NSDictionary type instance representing default paramaters and request headers respectively

+ (NSMutableDictionary *)commonParameters {
    return _commonParamDict;
}

+ (NSMutableDictionary *)requestHeader {
    return _requestHeader;
}
+(NSString*)getOriginalStringFromSpecialCharacter:(NSString *)myToken
{
    NSString  *token = myToken;
    @try
    {
        
        token = [token stringByReplacingOccurrencesOfString:@"%20" withString:@" "];
        token = [token stringByReplacingOccurrencesOfString:@"%26" withString:@"&"];
        token = [token stringByReplacingOccurrencesOfString:@"%2B" withString:@"+"];
        token = [token stringByReplacingOccurrencesOfString:@"%2c" withString:@","];
        token = [token stringByReplacingOccurrencesOfString:@"%28" withString:@"("];
        token = [token stringByReplacingOccurrencesOfString:@"%29" withString:@")"];
        token = [token stringByReplacingOccurrencesOfString:@"%21" withString:@"!"];
        token = [token stringByReplacingOccurrencesOfString:@"%3D" withString:@"="];
        token = [token stringByReplacingOccurrencesOfString:@"%3C" withString:@"<"];
        token = [token stringByReplacingOccurrencesOfString:@"%3E" withString:@">"];
        token = [token stringByReplacingOccurrencesOfString:@"%23" withString:@"#"];
        token = [token stringByReplacingOccurrencesOfString:@"%24" withString:@"$"];
        token = [token stringByReplacingOccurrencesOfString:@"%27" withString:@"'"];
        token = [token stringByReplacingOccurrencesOfString:@"%2A" withString:@"*"];
        token = [token stringByReplacingOccurrencesOfString:@"%2D" withString:@"-"];
        token = [token stringByReplacingOccurrencesOfString:@"%2E" withString:@"."];
        token = [token stringByReplacingOccurrencesOfString:@"%2F" withString:@"/"];
        token = [token stringByReplacingOccurrencesOfString:@"%3A" withString:@":"];
        token = [token stringByReplacingOccurrencesOfString:@"%3B" withString:@";"];
        token = [token stringByReplacingOccurrencesOfString:@"%3F" withString:@"?"];
        token = [token stringByReplacingOccurrencesOfString:@"%40" withString:@"@"];
        token = [token stringByReplacingOccurrencesOfString:@"%5B" withString:@"["];
        token = [token stringByReplacingOccurrencesOfString:@"%5C" withString:@"\\"];
        token = [token stringByReplacingOccurrencesOfString:@"%5D" withString:@"]"];
        token = [token stringByReplacingOccurrencesOfString:@"%5F" withString:@"_"];
        token = [token stringByReplacingOccurrencesOfString:@"%60" withString:@"`"];
        token = [token stringByReplacingOccurrencesOfString:@"%7B" withString:@"{"];
        token = [token stringByReplacingOccurrencesOfString:@"%7C" withString:@"|"];
        token = [token stringByReplacingOccurrencesOfString:@"%7D" withString:@"}"];
        return token;
    }
    @catch (NSException *exception) {
        
    }
    
}


#pragma mark - Clean up method

- (void)dealloc{
    
    //self.delegate = nil;
}

@end
