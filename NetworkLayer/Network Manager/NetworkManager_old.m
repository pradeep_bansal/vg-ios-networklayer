//
//  NetworkManager.m
//  NetworkLayer
//
//  Created by VS on 02/09/16.
//  Copyright © 2016 VS. All rights reserved.
//

#import <NetworkLayer/NetworkLayer.h>
#import <NetworkLayer/Reachability.h>
#import <ReactiveCocoa/ReactiveCocoa.h>
#import <NetworkLayer/CacheManager.h>
#import <AFNetworking.h>

#import <NetworkLayer/NetworkErrorLogger.h>


#define kNoUrlError @"No url specified!"
#define kEmptyString @""
#define kURLUnavailable @"Url \"%@\" unavailable", urlString
#define kAPIBodyKey @"bodystring"


static NSMutableDictionary *_commonParamDict =nil;
static NSMutableDictionary *_requestHeader = nil;


@interface NetworkManager()

+ (NSMutableDictionary *)commonParameters;

+ (NSMutableDictionary *)requestHeader;

- (NetworkStatus)getReachabiltyStatus;

- (NSError *)errorWithDomain:(NSString *)urlString
                        code:(NSInteger)code
                 description:(NSString *)description;

@end


@implementation NetworkManager {
    
    NSMutableDictionary *_parameters;
    CacheManager *_cacheManager;
    long int _httpStatusCode;
}


+ (void)initialize
{
    static dispatch_once_t once;
    dispatch_once(&once, ^{
        _commonParamDict = [[NSMutableDictionary alloc] init];
        _requestHeader = [[NSMutableDictionary alloc] init];
    });
}


- (instancetype)init {
    
    if (self = [super init]) {
        [[AFNetworkReachabilityManager sharedManager] startMonitoring];
    }
    return self;
}


#pragma mark - Public method that sets Common parameters for requests

+ (void)setServerRequestCommonParameters:(NSDictionary *)commonParametersDictionary {
    
    NSMutableDictionary *dict = [commonParametersDictionary  mutableCopy];
    
    NSArray *keys = [dict allKeys];
    for (NSString *key in keys) {
        [[NetworkManager commonParameters] setValue:[dict valueForKey:key]
                                             forKey:key];
    }
}


+ (void)setRequestHeaderWithDictionary:(NSDictionary *)headerDictionary {
    
    for (id headerKey in [NSArray arrayWithArray:[headerDictionary allKeys]]) {
        
        NSString *headerValue = [headerDictionary valueForKey:headerKey];
        [_requestHeader setValue:headerValue forKey:headerKey];
    }
}


#pragma mark - Instance methods to get the data from Server or cache

- (void)getServerDataWithUrl:(NSString *)urlString
           requestParameters:(NSDictionary *)params
              bodyParameters:(id)bodyParams
               apiMethodType:(NSString *)methodType
              andMapperClass:(Class)mapperClass
                  completion:(ResponseBlock)completionBlock {
    [self getServerDataWithUrl:urlString requestParameters:params bodyParameters:bodyParams apiMethodType:methodType andMapperClass:mapperClass andSerializer:NetworkManagerSerializerJSON completion:completionBlock];
}

- (void)getServerDataWithUrl:(NSString *)urlString
           requestParameters:(NSDictionary *)params
              bodyParameters:(id)bodyParams
               apiMethodType:(NSString *)methodType
              andMapperClass:(Class)mapperClass
               andSerializer:(NetworkManagerSerializer)serializer
                  completion:(ResponseBlock)completionBlock{
    
    dispatch_async(dispatch_get_main_queue(), ^{
        
        if ([self isUrlStringValid:urlString] == NO) {
            NSString *errorDescription = [NSString
                                          stringWithFormat:kUrlStringInvalidMsg];
            
            //TODO:: Discussion required over error codes
            NSError *error = [self errorWithDomain:urlString
                                              code:0
                                       description:errorDescription];
            
            [NetworkErrorLogger logNetworkError:error
                                        withUrl:urlString
                                   headerFields:_requestHeader
                                         params:params
                              andResponseObject:nil];
            
            completionBlock(nil, error);
            return;
        }
        
        if (_parameters != nil) {
            [_parameters  removeAllObjects];
        }
        
        if ([[params allKeys] count] > 0) {
            _parameters = [NSMutableDictionary dictionaryWithDictionary:params];
        }
        
        if (_parameters == nil) {
            _parameters = [[NSMutableDictionary alloc] init];
        }
        
        NSMutableDictionary *dict = [NetworkManager commonParameters];
        
        NSArray *keys = [dict allKeys];
        
        if ([keys count] > 0) {
            OALog(kCommonParametersSetMsg);
        }
        else {
            OALog(kCommonParametersNotSetMsg);
        }
        
        for (id key in keys) {
            if (![_requestHeader valueForKey:key]) {
                [_parameters setValue:[dict valueForKey:key]
                               forKey:key];
            }
        }
        
        
        /** Reactive Cocoa merge Operation on Cache Mapper
         *  response and Network Mapper reponse
         */
        RACSubject *cacheResponseSubject = [RACSubject subject];
        RACSubject *networkResponseSubject = [RACSubject subject];
        RACSignal *merged = [RACSignal
                             merge:@[cacheResponseSubject,
                                     networkResponseSubject]];
        [merged subscribeNext:^(NSObject *mapper) {
            completionBlock(mapper, nil);
            
            
        }];
        
        //get cache data
        _cacheManager = [[CacheManager alloc] init];
        [_cacheManager getCacheDataForMapperClass:mapperClass
                                    withRequestHeader:_requestHeader
                                requestParams:_parameters
                                       bodyParams:bodyParams
                                    apiMethodType:methodType
                                       completion:^(NSObject *mapper, NSError *error) {
                                           if (mapper) {
                                               [cacheResponseSubject sendNext:mapper];
                                           }
                                           else if (error) {
                                               completionBlock(nil, error);
                                           }
                                           else {
                                               NSString *errorDescription = [NSString
                                                                             stringWithFormat:kUnhandledErrorForCacheMsg];
                                               
                                               //TODO:: Discussion required over error codes
                                               NSError *error = [self errorWithDomain:urlString
                                                                                 code:0
                                                                          description:errorDescription];
                                               completionBlock(nil, error);
                                           }
                                       }];
        
        
        if([self getReachabiltyStatus] != NotReachable)
        {
            //            if([self.delegate respondsToSelector:@selector(internetAvailabilityWithStatus:)])
            //            {
            //                [self.delegate internetAvailabilityWithStatus:InternetReachable];
            //            }
            
            AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
            manager.responseSerializer = [AFJSONResponseSerializer serializer];
            
            if(serializer == NetworkManagerSerializerJSON){
                manager.requestSerializer = [AFJSONRequestSerializer serializer];
            }
            else{
                manager.requestSerializer = [AFHTTPRequestSerializer serializer];
            }
            
            for (NSString *key in _requestHeader) {
                if(serializer == NetworkManagerSerializerHTTP && [key.lowercaseString isEqualToString:@"content-type"]){
                    continue;
                }
                NSString *value = [_requestHeader valueForKey:key];
                OALog(@"Setting HTTPHeaderField : %@ Value : %@", key, value);
                [manager.requestSerializer setValue:value forHTTPHeaderField:key];
            }
            
            
            OALog(kRequestParametersMsg);
            
            if([methodType isEqualToString:kGETMethod])
            {
                [manager GET:urlString
                  parameters:_parameters
                    progress:nil
                     success:^(NSURLSessionTask *task, id responseObject) {
                         
                         NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *)task.response;
                         _httpStatusCode = (long)httpResponse.statusCode;
                         
                         OALog(kServiceCallSuccessForURLMsg);
                         OALog(kHTTPStatusCodeMsg);
                         
                         NSObject *mapper = [[mapperClass alloc] initWithJsonObject:responseObject];
                         
                         if(mapper)
                         {
                             OALog(kMapperMappedFromAPIResponseMsg);
                             
                             [_cacheManager writeMapperToCacheWithMapperClass:mapperClass
                                                                 mapperObject:mapper
                                                                apiMethodType:methodType
                                                                requestHeader:_requestHeader
                                                                    paramDict:_parameters
                                                                andBodyParams:bodyParams];
                             BOOL cacheInfoJSONFileUpdated = [_cacheManager updateCacheInfoJSONFile];
                             OALog(kCacheInfoJSONFileUpdatedMsg);
                             
                             [networkResponseSubject sendNext:mapper];
                         }
                         else
                         {
                          
                             if (_httpStatusCode == 200) {
                                 OALog(kModelParsingErrorMsg);
                             }
                             NSError *error = [self
                                               mapperNotFormedErrorForMapperClass:mapperClass
                                               withRequestUrl:urlString];
                             
                             [NetworkErrorLogger logNetworkError:error
                                                         withUrl:urlString
                                                    headerFields:_requestHeader
                                                          params:_parameters
                                               andResponseObject:responseObject];
                         }
                         
                     } failure:^(NSURLSessionTask *operation, NSError *error) {
                         
                         
                         NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *)operation.response;
                         _httpStatusCode = (long)httpResponse.statusCode;
                         
                         OALog(kGETServiceCallFailure);
                         OALog(kHTTPStatusCodeMsg);
                         
                         [NetworkErrorLogger logNetworkError:error
                                                     withUrl:urlString
                                                headerFields:_requestHeader
                                                      params:_parameters
                                           andResponseObject:nil];
                         
                         completionBlock(nil, error);
                     }];
                
            }
            
            //TODO:: //Write logic for HEAD method completion block
            else if ([methodType isEqualToString:kHEADMethod]) {
                
                [manager HEAD:urlString
                   parameters:_parameters
                      success:^(NSURLSessionDataTask * _Nonnull task) {
                          
                      }
                      failure:^(NSURLSessionDataTask * _Nullable task,
                                NSError * _Nonnull error) {
                          
                      }
                 ];
                
            }
            
            else if([methodType isEqualToString:kPOSTMethod]) {
                
                NSString *postUrlString = [urlString stringByAppendingString:@"?"];
                if(_parameters)
                {
                    NSInteger keyCount = 0;
                    NSInteger allKeysCount = [_parameters allKeys].count;
                    
                    for (NSString *key in [_parameters allKeys]) {
                        
                        keyCount = keyCount + 1;
                        NSString *valueForKey = [_parameters valueForKey:key];
                        
                        if(key && valueForKey)
                        {
                            postUrlString = [postUrlString stringByAppendingString:[NSString stringWithFormat:@"%@=%@",key,valueForKey]];
                        }
                        
                        if(allKeysCount > keyCount)
                        {
                            postUrlString = [postUrlString stringByAppendingString:@"&"];
                        }
                    }
                }
                
                [manager POST:postUrlString
                   parameters:bodyParams
                     progress:nil
                      success:^(NSURLSessionDataTask * _Nonnull task,
                                id  _Nullable responseObject) {
                          OALog(kServerSuccessResponseMsg);
                          
                          NSObject *mapper = [[mapperClass alloc]
                                              initWithJsonObject:responseObject];
                          
                          NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *)task.response;
                          _httpStatusCode = (long)httpResponse.statusCode;
                          OALog(kHTTPStatusCodeMsg);
                          
                          if(mapper) {
                              
                              OALog(kMapperMappedFromAPIResponseMsg);
                              
                              [_cacheManager writeMapperToCacheWithMapperClass:mapperClass
                                                                  mapperObject:mapper
                                                                 apiMethodType:methodType
                                                                 requestHeader:_requestHeader
                                                                     paramDict:_parameters
                                                                 andBodyParams:bodyParams];                              BOOL cacheInfoJSONFileUpdated = [_cacheManager updateCacheInfoJSONFile];
                              OALog(kCacheInfoJSONFileUpdatedMsg);
                              [networkResponseSubject sendNext:mapper];
                          }
                          else
                          {
                              NSError *error = [self
                                                mapperNotFormedErrorForMapperClass:mapperClass
                                                withRequestUrl:urlString];
                              
                              [NetworkErrorLogger logNetworkError:error
                                                          withUrl:urlString
                                                     headerFields:_requestHeader
                                                           params:bodyParams
                                                andResponseObject:responseObject];
                          }
                          
                      } failure:^(NSURLSessionDataTask * _Nullable task,
                                  NSError * _Nonnull error) {
                          OALog(kPOSTServiceCallFailure);
                          
                          NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *)task.response;
                          _httpStatusCode = (long)httpResponse.statusCode;
                          OALog(kHTTPStatusCodeMsg);
                          
                          [NetworkErrorLogger logNetworkError:error
                                                      withUrl:urlString
                                                 headerFields:_requestHeader
                                                       params:params
                                            andResponseObject:nil];
                          
                          completionBlock(nil, error);
                      }];
            }
            
            else if ([methodType isEqualToString:kPUTMethod]) {
                
                [manager PUT:urlString
                  parameters:bodyParams
                     success:^(NSURLSessionDataTask * _Nonnull task,
                               id  _Nullable responseObject) {
                         
                         NSObject *mapper = [[mapperClass alloc]
                                             initWithJsonObject:responseObject];
                         
                         NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *)task.response;
                         _httpStatusCode = (long)httpResponse.statusCode;
                         OALog(kHTTPStatusCodeMsg);
                         
                         if(mapper) {
                             
                             OALog(kMapperMappedFromAPIResponseMsg);
                             
                             [_cacheManager writeMapperToCacheWithMapperClass:mapperClass
                                                                 mapperObject:mapper
                                                                apiMethodType:methodType
                                                                requestHeader:_requestHeader
                                                                    paramDict:_parameters
                                                                andBodyParams:bodyParams];                             BOOL cacheInfoJSONFileUpdated = [_cacheManager updateCacheInfoJSONFile];
                             OALog(kCacheInfoJSONFileUpdatedMsg);
                             [networkResponseSubject sendNext:mapper];
                         }
                         else
                         {
                             
                             NSError *error = [self mapperNotFormedErrorForMapperClass:mapperClass
                                                                        withRequestUrl:urlString];
                             
                             [NetworkErrorLogger logNetworkError:error
                                                         withUrl:urlString
                                                    headerFields:_requestHeader
                                                          params:bodyParams
                                               andResponseObject:responseObject];
                         }
                         
                     }
                     failure:^(NSURLSessionDataTask * _Nullable task,
                               NSError * _Nonnull error) {
                         
                         NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *)task.response;
                         _httpStatusCode = (long)httpResponse.statusCode;
                         OALog(kHTTPStatusCodeMsg);
                         
                         completionBlock(nil, error);
                     }
                 ];
            }
            
            else if ([methodType isEqualToString:kDELETEMethod]) {
                
                [manager DELETE:urlString
                     parameters:_parameters
                        success:^(NSURLSessionDataTask * _Nonnull task,
                                  id  _Nullable responseObject) {
                            NSObject *mapper = [[mapperClass alloc]
                                                initWithJsonObject:responseObject];
                            NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *)task.response;
                            _httpStatusCode = (long)httpResponse.statusCode;
                            OALog(kHTTPStatusCodeMsg);
                            
                            if(mapper) {
                                
                                OALog(kMapperMappedFromAPIResponseMsg);
                                [_cacheManager writeMapperToCacheWithMapperClass:mapperClass
                                                                    mapperObject:mapper
                                                                   apiMethodType:methodType
                                                                   requestHeader:_requestHeader
                                                                       paramDict:_parameters
                                                                   andBodyParams:bodyParams];
                                BOOL cacheInfoJSONFileUpdated = [_cacheManager updateCacheInfoJSONFile];
                                OALog(kCacheInfoJSONFileUpdatedMsg);
                                [networkResponseSubject sendNext:mapper];
                            }
                            else
                            {
                                
                                NSError *error = [self mapperNotFormedErrorForMapperClass:mapperClass
                                                                           withRequestUrl:urlString];
                                
                                [NetworkErrorLogger logNetworkError:error
                                                            withUrl:urlString
                                                       headerFields:_requestHeader
                                                             params:bodyParams
                                                  andResponseObject:responseObject];
                            }
                            
                            
                        }
                        failure:^(NSURLSessionDataTask * _Nullable task,
                                  NSError * _Nonnull error) {
                            NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *)task.response;
                            _httpStatusCode = (long)httpResponse.statusCode;
                            OALog(kHTTPStatusCodeMsg);
                            
                            completionBlock(nil, error);
                        }
                 ];
            }
        }
        else {
            
            OALog(kNetworkNotReachableError);
            NSError *error = [self internetNotAvailableError];
            
            completionBlock(nil, error);
            
        }
    });
}



#pragma mark - Helper instance methods for getServerDataForMapper method

- (BOOL)isUrlStringValid:(NSString *)urlString {
    
    if(urlString){
        OALog(kUrlStringValidatedMsg);
        
        return YES;
    }
    OALog(kUrlStringInvalidMsg);
    return NO;
}


#pragma mark - Private Helper Instance methods to return specific NSError instance

- (NSError *)mapperNotFormedErrorForMapperClass:(Class)mapperClass
                                 withRequestUrl:(NSString *)urlString {
    
    NSString *errorDescription = [NSString
                                  stringWithFormat:kMapperCouldNotBeMappedFromAPIResponseMsg];
    
    return [self errorWithDomain:urlString
                            code:0
                     description:errorDescription];
}


- (NSError *)internetNotAvailableError {
    
    NSString *errorDescription = kInternetNotAvailable;
    
    return [self errorWithDomain:kInternetNotAvailableError
                            code:2059
                     description:errorDescription];
}


/**
 *
 * Private helper method that returns an NSError type instance
 * @parameters: urlString -- The url string for which this error is getting created
 * code -- error code
 * description -- description of the error
 *
 */
- (NSError *)errorWithDomain:(NSString *)urlString
                        code:(NSInteger)code
                 description:(NSString *)description {
    
    //TODO:: Discussion required over error codes
    
    NSMutableDictionary* details = [NSMutableDictionary dictionary];
    
    //If no url is specified
    if (urlString.length == 0 || urlString == nil) {
        urlString = kEmptyString;
        description = kNoUrlError;
    }
    
    [details setValue:description
               forKey:NSLocalizedDescriptionKey];
    
    return [NSError errorWithDomain:urlString
                               code:code
                           userInfo:details];
}


#pragma mark - Internet Reachability

- (NetworkStatus)getReachabiltyStatus {
    
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [reachability currentReachabilityStatus];
    
    return networkStatus;
    
}


#pragma mark - Private class methods returning NSDictionary type instance representing default paramaters and request headers respectively

+ (NSMutableDictionary *)commonParameters {
    return _commonParamDict;
}

+ (NSMutableDictionary *)requestHeader {
    return _requestHeader;
}


#pragma mark - Clean up method

- (void)dealloc{
    
    //self.delegate = nil;
}

@end
