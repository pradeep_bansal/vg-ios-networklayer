//
//  NetworkErrorLogger.h
//  NetworkLayer
//
//  Created by VS on 10/09/16.
//  Copyright © 2016 VS. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface NetworkErrorLogger : NSObject


+ (void)logNetworkError:(NSError *)error
                withUrl:(NSString *)url
           headerFields:(NSDictionary *)headerFields
              params:(id)params
      andResponseObject:(id)responseObj;



@end
