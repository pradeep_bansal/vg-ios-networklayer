//
//  CacheManager.m
//  NetworkLayer
//
//  Created by VS on 09/11/16.
//  Copyright © 2016 VS. All rights reserved.
//

#import <NetworkLayer/CacheManager.h>
#import <NetworkLayer/NSString+MD5Crypto.h>
#import <NetworkLayer/NSObject+Cache.h>
#import <NetworkLayer/NSFileManager+NRFileManager.h>
#import <NetworkLayer/NetworkLayer.h>
#import "NSData+MD5Crypto.h"


#define kCacheFilesFileName @"CacheFiles"
#define kFileTypeJSON @"txt"
#define kCacheFilesKey @"cacheFiles"
#define kPlainFileNameKey @"plainFileName"
#define kCacheFileNameKey @"cacheFileName"


#define kAPIMethodGET @"GET"
#define kAPIMethodPOST @"POST"
#define kAPIResponseCacheDirectoryTitle @"APIResponseCache"
#define kCacheInfoJSONDirectory @"CacheInfoJSON"

static NSUInteger _maximumNumberOfCacheFiles;
static unsigned long long _cacheSize;

@interface CacheManager () {
    
}

@property (nonatomic, strong) NSArray * cachedFileList;

#pragma  mark - Private Methods declaration

+ (void)initialize;


- (NSString *)encryptedFileNameForMapperClass:(Class)mapperClass
                                      withUrl:(NSString *)url
                                requestHeader:(NSDictionary *)header
                                    paramDict:(NSDictionary *)params
                                   bodyParams:(id)bodyParams
                                apiMethodType:(NSString *)methodType;

- (NSString *)fileNameForMapperClass:(Class)mapperClass
                             withUrl:(NSString *)url
                       requestHeader:(NSDictionary *)header
                           paramDict:(NSDictionary *)dictionary
                          bodyParams:(id)bodyParams
                       apiMethodType:(NSString *)methodType;



- (BOOL)isCacheFullForArray:(NSArray *)cacheFilesArray;

- (BOOL)isMaximumCacheSizeLimitReached;

@end



@implementation CacheManager
@synthesize cachedFileList = _cachedFileList;

+ (void)initialize {
    static dispatch_once_t once;
    dispatch_once(&once, ^{
        
        [CacheManager setAllowedCacheSize:(2*1024*1024)];
        [CacheManager setMaximumFilesInCache:100];
    });
    
}

-(NSArray *)cachedFileList{
    NSString *path = [CacheManager apiCacheDirectoryPath];
    NSArray *array = [[NSFileManager defaultManager] contentsOfDirectoryAtPath:path error:NULL];
    if(array == nil)
        array = [NSArray array];
    return array;
}


#pragma mark - Public methods

+ (void)setAllowedCacheSize:(unsigned long long)cacheSize {

    _cacheSize = cacheSize;
}


+ (unsigned long long)cacheSize {
    
    return  _cacheSize;
}

+ (NSUInteger)maximumNumberOfFilesInCache {
    
    return _maximumNumberOfCacheFiles;
}



+ (void)setMaximumFilesInCache:(NSUInteger)maximumCacheFiles {
    
    _maximumNumberOfCacheFiles = maximumCacheFiles;
}


- (void)getCacheDataForMapperClass:(Class)mapperClass
                 withUrl:(NSString *)url
                 requestHeader:(NSDictionary *)header
                     requestParams:(NSDictionary *)params
                        bodyParams:(id)bodyParams
                     apiMethodType:(NSString *)methodType
                        completion:(CacheResponseBlock)completionBlock {
    
    if([mapperClass allowCaching]){
        
        OALog(kCachingAllowedMsg);
        OALog(kCheckingCacheMsg);
        
        
        NSString *cacheFileName = [self encryptedFileNameForMapperClass:mapperClass
                                                       withUrl:url
                                                 requestHeader:header
                                                     paramDict:params
                                                    bodyParams:bodyParams
                                                 apiMethodType:methodType];
    
        if ([self.cachedFileList count] > 0) {
            BOOL fileExistsInCache = NO;
            if ([self.cachedFileList containsObject:cacheFileName]) {
                NSString *filePath = [[[self class] apiCacheDirectoryPath]
                                      stringByAppendingPathComponent:cacheFileName];
                
                NSFileManager *manager = [NSFileManager defaultManager];
                if([manager fileExistsAtPath:filePath]){
                    fileExistsInCache = YES;
                    [self readCacheForMapperClass:mapperClass
                                       atFilePath:filePath
                                       completion:^(id jsonObject, NSString *hashString, NSError *error) {
                                           
                                           id mapper  = [mapperClass alloc];
                                           if(mapperClass != [NSDictionary class] && [mapper respondsToSelector:@selector(initWithDictionary:)]){
                                               mapper = [mapper initWithDictionary:jsonObject];
                                           }
                                           else{
                                               mapper = [[mapperClass alloc] initWithJsonObject:jsonObject];
                                           }
                                           
                                           if (mapper) {
                                               OALog(kMapperMappedFromCacheMsg);
                                               
                                               completionBlock(mapper, hashString, nil);
                                           }
                                           else {
                                               OALog(kCacheMappingError);
                                               completionBlock(nil, nil, error);
                                           }
                                       }];
                    
                    
                }
                else{
                    NSError *error = [NSError errorWithDomain:kNoFilesInCacheDirectory
                                                         code:0
                                                     userInfo:nil];
                    completionBlock(nil, nil, error);
                }
            }
            else if (fileExistsInCache == NO) {
                
                NSString *errorDescription = [NSString stringWithFormat:kCacheFileNonExistentError];
                
                NSMutableDictionary* details = [NSMutableDictionary dictionary];
                
                [details setValue:errorDescription
                           forKey:NSLocalizedDescriptionKey];
                
                NSError *error = [NSError errorWithDomain:kCacheFileNonExistentError
                                                     code:0
                                                 userInfo:details];
                OALog(kAboutToSendErrorToCompletionBlockMsg);
                completionBlock(nil, nil, error);
                
            }
        }
        else if ([self.cachedFileList count] == 0) {
            
            OALog(kNoFilesInCacheDirectory);
            NSError *error = [NSError errorWithDomain:kNoFilesInCacheDirectory
                                                 code:0
                                             userInfo:nil];
            completionBlock(nil, nil, error);
        }
    }
    else {
        NSString *errorDescription = [NSString stringWithFormat:kCachingNotAllowedError];
        
        NSMutableDictionary* details = [NSMutableDictionary dictionary];
        
        [details setValue:errorDescription
                   forKey:NSLocalizedDescriptionKey];
        
        NSError *error = [NSError errorWithDomain:kCachingNotAllowedError
                                             code:0
                                         userInfo:details];
        OALog(kAboutToSendErrorToCompletionBlockMsg);
        completionBlock(nil, nil, error);
    }
    
}


#pragma mark - Helper methods to get Model from CacheInfo JSON File



#pragma mark - Private Helper methods relted to fileName

- (NSString *)encryptedFileNameForMapperClass:(Class)mapperClass
                                      withUrl:(NSString *)url
                               requestHeader:(NSDictionary *)header
                                    paramDict:(NSDictionary *)params
                                   bodyParams:(id)bodyParams
                                apiMethodType:(NSString *)methodType {
    
    NSString *plainFileName = [self fileNameForMapperClass:mapperClass
                                          withUrl:url
                                    requestHeader:header
                                        paramDict:params
                                       bodyParams:bodyParams
                                    apiMethodType:methodType
                      ];
    
    NSString *encrptedFileName = [plainFileName MD5String];
    
    return [NSString stringWithFormat:@"%@.%@", encrptedFileName, kFileTypeJSON];
}


//TODO:: Write implementation for this method
- (NSString *)fileNameForMapperClass:(Class)mapperClass
                             withUrl:(NSString *)url
                       requestHeader:(NSDictionary *)header
                           paramDict:(NSDictionary *)dictionary
                          bodyParams:(id)bodyParams
                       apiMethodType:(NSString *)methodType{
    
    NSString *fileName = NSStringFromClass(mapperClass);
    NSString *strUrl = [self getStringNameFromObject:url];
    fileName = [fileName stringByAppendingFormat:@"_%@", strUrl];
    NSString *strHeaders = [self getStringNameFromObject:header];
    fileName = [fileName stringByAppendingFormat:@"_%@", strHeaders];
    NSString *strMethod = [self getStringNameFromObject:methodType];
    fileName = [fileName stringByAppendingFormat:@"_%@", strMethod];
    NSString *strParam = [self getStringNameFromObject:dictionary];
    fileName = [fileName stringByAppendingFormat:@"_%@", strParam];
    NSString *strBodyParameters = [self getStringNameFromObject:bodyParams];
    fileName = [fileName stringByAppendingFormat:@"_%@",strBodyParameters];

    return fileName;
}

-(NSString *)getStringNameFromObject:(id)object{
    if(object == nil){
        return @"";
    }
    else if([object isKindOfClass:[NSString class]]){
        return object;
    }
    else if ([object isKindOfClass:[NSDictionary class]]){
        NSDictionary *dictData = (NSDictionary *)object;
        NSArray *allKeys = [dictData allKeys];
        NSMutableArray *arrNames = [NSMutableArray array];
        for(NSString *key in allKeys){
            id value  = [dictData objectForKey:key];
            NSString *strValue = [self getStringNameFromObject:value];
            NSString *strKeyValue = [NSString stringWithFormat:@"%@_%@",key, strValue];
            [arrNames addObject:strKeyValue];
        }
        NSString *strFinalName = [arrNames componentsJoinedByString:@"_"];
        return strFinalName;
    }
    else if ([object isKindOfClass:[NSArray class]]){
        NSMutableArray *arrNames = [NSMutableArray array];
        for(id data in object){
            NSString *strValue = [self getStringNameFromObject:data];
            [arrNames addObject:strValue];
        }
        NSString *strFinalName = [arrNames componentsJoinedByString:@"_"];
        return strFinalName;
    }
    return @"";
}


#pragma mark - Private Helper methods for checking cache size

- (BOOL)isCacheFullForArray:(NSArray *)cacheFilesArray {
    
    BOOL isCacheFull = NO;
    NSInteger cacheFilesCount = [cacheFilesArray count];
    if ([cacheFilesArray count] > 0) {
        
        if (cacheFilesCount >= _maximumNumberOfCacheFiles || [self isMaximumCacheSizeLimitReached]) {
            isCacheFull = YES;
        }
    }
    return isCacheFull;
}



- (BOOL)isMaximumCacheSizeLimitReached {
    
    BOOL isCacheSizeLimitReached = NO;
    
    NSFileManager *fileManager = [NSFileManager defaultManager];
    unsigned long long size = 0;
    NSError *error;
    
    NSURL *cacheDirectoryURL = [NSURL fileURLWithPath:[[self class] apiCacheDirectoryPath]];
    
    [fileManager nr_getAllocatedSize:&size
                    ofDirectoryAtURL:cacheDirectoryURL
                               error:&error];
    
    if (size >= _cacheSize) {
        isCacheSizeLimitReached = YES;
    }
    else if (error) {
        OALog(kDirectorySizeCalculationError);
    }
    return isCacheSizeLimitReached;
}



#pragma mark - Worker methods for cache read/write/deletion

- (void)readCacheForMapperClass:(Class)mapperClass
                     atFilePath:(NSString *)filePath
                     completion:(CacheResponseBlock)completionBlock {
    
    NSError *errorMappingCacheToMapperMsg;
    
    NSData *cashedData = [NSData dataWithContentsOfFile:filePath];
    
    NSObject *json = [NSJSONSerialization JSONObjectWithData:cashedData
                                                     options:0
                                                       error:&errorMappingCacheToMapperMsg];
    NSString *hashString = [cashedData MD5String];
    if(errorMappingCacheToMapperMsg) {
        completionBlock(nil, nil, errorMappingCacheToMapperMsg);
        return;
    }
    
    if (json) {
        completionBlock(json, hashString, nil);
    }
    else {
        OALog(kUncaughtErrorInReadingCacheMsg);
        NSError *error = [NSError errorWithDomain:@"Uncaught exception while reading file" code:0 userInfo:nil];
        completionBlock(nil, nil, error);
    }
}


//Public Helper method to write json to cache
- (void)writeMapperToCacheWithMapperClass:(Class)mapperClass
                             responseData:(NSData *)responseData
                                  url:(NSString *)url
                            apiMethodType:(NSString *)methodType
                           requestHeader:(NSDictionary *)header
                                paramDict:(NSDictionary *)params
                            andBodyParams:(id)bodyParams {
    
    if([mapperClass allowCaching] && responseData != nil){
        OALog(kCachingAllowedMsg);
        OALog(kWritingToCacheMsg);
        
        @synchronized (self) {
            // do stuff...

            NSString *encryptedFileName = [self encryptedFileNameForMapperClass:mapperClass
                                                                        withUrl:url
                                                                  requestHeader:header
                                                                      paramDict:params
                                                                     bodyParams:bodyParams
                                                                  apiMethodType:methodType];
            
            //Check cache
            NSString *filePath = [[CacheManager apiCacheDirectoryPath]
                                  stringByAppendingPathComponent:encryptedFileName];
        
            BOOL fileDeleted = [self performLRUBasedCacheFileDeletionIfNeeded];
            
            OALog(kCacheFileDeletionStatusMsg);
            
            BOOL success = [responseData writeToFile:filePath atomically:YES];
    
            if (success) {
                NSLog(@"Successfully written cache at filePath: %@",filePath);
            }
            else {
                NSLog(@"Error in writing cache file at path: %@.",filePath);
            }
        };
    }
}


- (BOOL)performLRUBasedCacheFileDeletionIfNeeded {
    
    if([self isCacheFullForArray:self.cachedFileList] == NO){
        return NO;
    }
    
    NSString *dirPath = [CacheManager apiCacheDirectoryPath];
    NSString *oldestFilePath = nil;
    NSDate *oldFileDate = [NSDate date];
    for(NSString *fileName in self.cachedFileList){
        NSString *filePath = [dirPath stringByAppendingPathComponent:fileName];
        NSDictionary *attributes = [[NSFileManager defaultManager] attributesOfItemAtPath:filePath error:nil];
        NSDate *date = [attributes fileModificationDate];
        if([oldFileDate compare:date] == NSOrderedDescending){
            oldestFilePath = filePath;
            oldFileDate = date;
        }
    }
    
    BOOL success = NO;
    NSError *error;
    if(oldestFilePath){
        NSFileManager *fileManager = [NSFileManager defaultManager];
        success = [fileManager removeItemAtPath:oldestFilePath error:&error];
    }
    if (success) {
        NSLog(@"Successfully deleted cache file. %@", oldestFilePath);
    }
    else if (error){
        NSLog(@"Error deleting cache file: %@. Error: %@", oldestFilePath, error );
    }
    return success;
}

#pragma mark - Private Helper method that returns JSON String by taking NSDictionary as input

-(NSString*) jsonStringFromJsonObject:(NSDictionary *)jsonObject {
    NSError *error;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:jsonObject
                                                       options:0
                                                         error:&error];
    
    if (! jsonData) {
        OALog(@"jsonStringFromJsonObject: error: %@", error.localizedDescription);
        return @"{}";
    } else {
        return [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    }
}


#pragma mark - Private Helper Class methods to get directory path

+ (NSString *) applicationDocumentsDirectory {
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,
                                                         NSUserDomainMask,
                                                         YES);
    NSString *basePath = ([paths count] > 0) ? [paths objectAtIndex:0] : nil;
    return basePath;
}


+ (NSString *)apiCacheDirectoryPath {
    NSString *directoryPath = [[self applicationDocumentsDirectory]
                               stringByAppendingPathComponent:kAPIResponseCacheDirectoryTitle];
    
    if([[NSFileManager defaultManager] fileExistsAtPath:directoryPath] == false){
        [[NSFileManager defaultManager]
         createDirectoryAtPath:directoryPath
         withIntermediateDirectories:true attributes:nil error:nil];
    }
    
    return directoryPath;
}

+(void)cleanCacheFiles {
    NSString *path = [self apiCacheDirectoryPath];
    BOOL success = [[NSFileManager defaultManager] removeItemAtPath:path error:nil];
    if(success == YES){
        NSLog(@"*********Response cache files clean successfully*********");
    }
    else{
        NSLog(@"*********Error !!! In cleaning Response cache files*********");
    }
}

@end
