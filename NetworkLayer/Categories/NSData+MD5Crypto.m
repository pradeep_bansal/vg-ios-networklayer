//
//  NSData+MD5Crypto.m
//  NetworkLayer
//
//  Created by Vasim Akram on 4/24/17.
//  Copyright © 2017 VS. All rights reserved.
//

#import "NSData+MD5Crypto.h"
#import <CommonCrypto/CommonDigest.h>

@implementation NSData (MD5Crypto)

- (NSString*)MD5String
{
    // Create byte array of unsigned chars
    unsigned char md5Buffer[CC_MD5_DIGEST_LENGTH];
    
    // Create 16 byte MD5 hash value, store in buffer
    CC_MD5(self.bytes, (int)self.length, md5Buffer);
    
    // Convert unsigned char buffer to NSString of hex values
    NSMutableString *output = [NSMutableString stringWithCapacity:CC_MD5_DIGEST_LENGTH * 2];
    for(int i = 0; i < CC_MD5_DIGEST_LENGTH; i++)
        [output appendFormat:@"%02x",md5Buffer[i]];
    
    return output;
}

@end
