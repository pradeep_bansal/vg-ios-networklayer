//
//  NSObject+Cache.m
//  NetworkLayer
//
//  Created by VS on 02/09/16.
//  Copyright © 2016 VS. All rights reserved.
//

#import <NetworkLayer/NSObject+Cache.h>
#import <objc/runtime.h>

#define kDefaultFileName @"default"

@implementation NSObject (Cache)


+ (NSString *)fileNameForParamDict:(NSDictionary *)dictionary {
    
    return kDefaultFileName;
}


+ (NSString *)fileNameForParamDict:(NSDictionary *)dictionary
                           andBody:(id)bodyParameters {
    
    return kDefaultFileName;
}



+ (BOOL)allowCaching {
    return YES;
}

+ (BOOL)printLogs {
    return NO;
}


@end
