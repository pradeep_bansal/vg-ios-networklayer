//
//  NSData+MD5Crypto.h
//  NetworkLayer
//
//  Created by Vasim Akram on 4/24/17.
//  Copyright © 2017 VS. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSData (MD5Crypto)

- (NSString*)MD5String;

@end
