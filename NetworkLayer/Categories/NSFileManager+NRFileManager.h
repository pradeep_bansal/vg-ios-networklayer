//
//  NSFileManager+NRFileManager.h
//  CacheManager
//
//  Created by VS on 09/11/16.
//  Copyright © 2016 VS. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSFileManager (NRFileManager)

- (BOOL)nr_getAllocatedSize:(unsigned long long *)size
           ofDirectoryAtURL:(NSURL *)directoryURL
                      error:(NSError * __autoreleasing *)error;

@end
