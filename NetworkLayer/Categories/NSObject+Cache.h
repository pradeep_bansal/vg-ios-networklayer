//
//  NSObject+Cache.h
//  NetworkLayer
//
//  Created by VS on 02/09/16.
//  Copyright © 2016 VS. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSObject (Cache)


/*
 * Override these methods in the Model corresponding to an API call (subclas of NSObject)
 */


/**
 * This method will be called by Cache manager in case the method request type is GET
 * This method should return the json file name without extension where cached data needs to be stored
 * 
 * If this method is not overriden then it returns @"default"
 */
+ (NSString *)fileNameForParamDict:(NSDictionary *)dictionary;


/**
 * This method will be called by Cache manager in case the method request type is POST
 *
 * If this method is not overriden then it returns @"default"
 */
+ (NSString *)fileNameForParamDict:(NSDictionary *)dictionary
                           andBody:(id)bodyParameters;

/**
 * Return whether you want to allow caching for a particular api call corresponding to the mapper class in which you implement/override this method
 * If not overridden, returns YES by Default
 */
+ (BOOL)allowCaching;
+ (BOOL)printLogs;

@end
