//
//  NetworkConstants.h
//  NetworkLayer
//
//  Created by VS on 02/09/16.
//  Copyright © 2016 VS. All rights reserved.
//

#ifndef NetworkConstants_h
#define NetworkConstants_h
#define kMapperCacheDirectoryTitle @"MapperCache"
#define kFileTypeJson @"json"

typedef void (^ResponseBlock)(NSObject *mapper, NSString *authorization, NSError *error);
typedef void (^CacheResponseBlock) (id jsonObject, NSString *hashString, NSError *error);

typedef enum {
    InternetNotReachable = 0,
    InternetReachable
} InternetReachabilityStatus;

#define kInternetNotAvailableError @"InternetNotAvailableError"

#define development 1

//Macros to disable logs on prod
#if development

#define OALog(...) NSLog(__VA_ARGS__)

#else

#define OALog(...)

#endif


#endif /* NetworkConstants_h */
